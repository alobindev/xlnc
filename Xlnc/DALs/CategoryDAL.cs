﻿using AloFramework.Core.DALs;
using Xlnc.Controllers;
using Xlnc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xlnc.DALs
{
    public class CategoryDAL : AbstractDAL<Category>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MeetingController));
        public void Save(Category category)
        {
            try
            {
                this.dbContext.Save(category);
            }
            catch (Exception e)
            {
                logger.Info("Error Message in Meetin DAL" + e.Message);
            }
        }
        public void Update(Category category)
        {
            try
            {
                category.Status = 1;
                this.dbContext.Update(category);
            }
            catch (Exception e)
            {
                logger.Info("Error Message in Meetin DAL" + e);
            }
        }

        public bool SaveMemberEntry(MemberEntry memberEntry)
        {

            try
            {
                if (memberEntry.MemberEntryID > 0)
                {

                    this.dbContext.Update("MemberEntries", "MemberEntryID", memberEntry);
                    //this.dbContext.Update(product);
                }
                else
                {

                    this.dbContext.Insert("MemberEntries", "MemberEntryID", memberEntry);
                    //this.dbContext.Insert(product);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Info("error Message in Product DAL" + e.Message);
                return false;
            }
        }



        public bool getMemberExists(MemberEntry memberEntry)
        {
            var member = this.dbContext.Fetch<MemberEntry>("Select * From MemberEntries where  MemberNo = @0 AND MeetingID = @1", memberEntry.MemberNo, memberEntry.MeetingID);
            if (member.Count > 0)
            {
                return true;
            }
            return false;
        }

        public Category FindByID(long ID)
        {
            return this.dbContext.First<Category>("where CategoryID = @0", ID);
        }

        public int Delete(long ID)
        {
            int result = 0;
            Category category = this.FindByID(ID);

            if (category != null)
            {
                category.Status = 0;
                this.dbContext.Delete(category);
                result = 1;
            }
            return result;
        }

        public List<Category> GetCategoriesByType(string categorytype)
        {

            var category = this.dbContext.Fetch<Category>("Where CategoryType = @0 and Status = @1", categorytype,1);
            return category;
        }
        /* public Category GetMeeting(long meetingID)
        {
            return this.dbContext.FetchO
        }*/
    }

}