﻿using AloFramework.Core.DALs;
using Xlnc.Controllers;
using Xlnc.Models;
using System;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Web;
using PetaPoco;

namespace Xlnc.DALs
{
    public class RoomDAL : AbstractDAL<Room>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RoomDAL));



        public List<Room> GetRooms()
        {
            var query = new Sql("Select * from Rooms");
            return this.dbContext.Fetch<Room>(query);
        }

        public void SaveBooking(Member member)
        {
            try
            {
                logger.Info("Trying to save Bookings, data = " + Newtonsoft.Json.JsonConvert.SerializeObject(member));
                var booking = new Booking();
                booking.MemberNo = member.MemberNo;
                booking.RoomNo = member.RoomNo;
                booking.LockerNo = member.LockerNo;
                booking.BookedDate = DateTime.Now;
                booking.CreatedAt = DateTime.Now;
                booking.UpdatedAt = DateTime.Now;
                booking.Status = "Booked";
                this.dbContext.Save(booking);

                var updateRoomsTable = new Sql("Update Rooms set IsBooked = @0, MemberID = @1 where RoomNo = @2 and LockerNo = @3", 1, member.MemberID, member.RoomNo, member.LockerNo);
                this.dbContext.Execute(updateRoomsTable);
            }
            catch (Exception ex)
            {
                logger.Error("Problem in saving Booking, due to " + ex);
            }
        }

        public List<Room> GetAvailableRooms(string filter)
        {
            var query = new Sql("Select * from Rooms where Concat(RoomNo, '', LockerNo) like @0 and IsBooked = @1", "%" + filter + "%", 0);
            return this.dbContext.Fetch<Room>(query);
        }

        public List<Room> GetAllRooms(string filter)
        {
            var query = new Sql("Select * from Rooms where Concat(RoomNo, '', LockerNo) like @0", "%" + filter + "%");
            return this.dbContext.Fetch<Room>(query);
        }

        public List<Room> GetOccupiedRooms(string filter)
        {
            var query = new Sql("Select * from Rooms where Concat(RoomNo, '', LockerNo) like @0 and IsBooked = @1", "%" + filter + "%", 1);
            return this.dbContext.Fetch<Room>(query);
        }

        public void RoomTransfer(RoomTransfer roomTransfer)
        {
            this.dbContext.Save(roomTransfer);
            this.dbContext.Execute(@"Update Members Set RoomNo = @0, LockerNo = @1 where MemberID = @2"
                                        , roomTransfer.ToRoomNo
                                        , roomTransfer.ToLockerNo
                                        , roomTransfer.MemberID);
            this.dbContext.Execute(@"Update Bookings Set RoomNo = @0, LockerNo = @1, UpdatedAt = @2 where MemberNo = @3"
                                        , roomTransfer.ToRoomNo
                                        , roomTransfer.ToLockerNo
                                        , DateTime.Now
                                        , roomTransfer.MemberNo);
            this.dbContext.Execute(@"Update Rooms Set IsBooked = @0, MemberID = @1 where RoomNo = @2 and LockerNo = @3"
                                        , 0
                                        , null
                                        , roomTransfer.FromRoomNo
                                        , roomTransfer.FromLockerNo);
            this.dbContext.Execute(@"Update Rooms Set IsBooked = @0, MemberID = @1 where RoomNo = @2 and LockerNo = @3"
                                        , 1
                                        , roomTransfer.MemberID
                                        , roomTransfer.ToRoomNo
                                        , roomTransfer.ToLockerNo);
        }

    }
}