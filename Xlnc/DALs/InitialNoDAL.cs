﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.Models;
using AloFramework.Core.DALs;
using PetaPoco;

namespace Xlnc.DALs
{
    public class InitialNoDAL : AbstractDAL<InitialNumber>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InitialNumber));
        public IEnumerable<InitialNumber> All()
        {
            return this.dbContext.Query<InitialNumber>("select * from InitialNumbers");          
        }

        public InitialNumber GetInitialNo(string ResourceType)
        {
            try
            {
                return this.dbContext.First<InitialNumber>("SELECT * FROM initialnumbers where ResourceType = @0", ResourceType);
            }
            catch (Exception e) {
                logger.Info("Error in GetInitialNo "+e.Message);
            }
            return null;
        }

        public void UpdateInitialNo(int updatedvalue, string ResourceType) {
            logger.Info("Inside UpdateInitialNo DAL Value=" + updatedvalue + " ResourceType =" + ResourceType);
            this.dbContext.Update<InitialNumber>("SET LastNo = @0 WHERE ResourceType = @1", updatedvalue, ResourceType);
        }
    }
}