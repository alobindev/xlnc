﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Xlnc.Models;
using PetaPoco;

namespace Xlnc.DALs
{
    public class MemberDAL : AbstractDAL<Member>
    {

        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MemberDAL));

        public IEnumerable<Member> All()
        {
            return this.dbContext.Query<Member>("select * from Members where Stauts =@0","1");
            //return this.dbContext.Page<Member>(page, itemsPerPage, "select * from Members", new { });            
        }

        public bool Save(Member member)
        {
            try
            {
                this.dbContext.Save(member);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Problem in saving Member, due to " + ex);
            }
            return false;
        }


        public void Update(Member member)
        {
            this.dbContext.Update(member);

        }

        public int Delete(long ID)
        {
            int result = 0;
            Member member = this.FindByID(ID);

            if (member != null)
            {
              member.Status = 0;
              this.dbContext.Update(member);
              result = 1;
            }
            return result;
        }

        public List<Member> GetBookedMembers(string filter)
        {
            /*var query = new Sql(@"Select Members.*, Bookings.* 
                                    from Members 
                                        Left Join Bookings on Bookings.MemberNo = Members.MemberNo
                                            where Members.MemberNo like @0 and
                                                Bookings.Status = @1"
                                                    , "%" + MemberNo + "%"
                                                    , "Booked");*/
            /*var query = new Sql(@"Select Bookings.*, Members.* 
                                    from Bookings 
                                        Left Join Members on Members.MemberNo = Bookings.MemberNo
                                            where Bookings.MemberNo like @0 and
                                                Bookings.Status = @1"
                                                    , "%" + MemberNo + "%"
                                                    , "Booked");*/
            var query = new Sql(@"Select * from Members Where Status = @0 and MemberNo like @1"
                                            , 1
                                            , "%" + filter + "%");
            return this.dbContext.Fetch<Member>(query);
        }

        public int Dismiss(long ID, string Reason)
        {
            int result = 0;
            Member member = this.FindByID(ID);

            if (member != null)
            {
                member.Status = 2;
                member.DismissDate = DateTime.Now.Date;
                member.DismissReason = Reason;
                this.dbContext.Update(member);
                result = 1;
            }
            return result;
        }

        public Member FindByID(long ID)
        {
            return this.dbContext.First<Member>(" where MemberID = @0", ID);
        }

        public Member GetMemberByMemberNo(long MemberNo)
        {
            return this.dbContext.First<Member>(" where MemberNo = @0", MemberNo);
        }

        public Member FindByMemberNo(long ID)
        {
            return this.dbContext.First<Member>(" where MemberNo = @0", ID);
        }

        /*public List<Member> GetMembershipNo(long ID)
        {
            return this.dbContext.Fetch<Member>("where MemberNo like @0 or  MobileNo like @1 and Status = @2", "%"+ ID,  "%"+ID, "1");
        }*/

        public List<Member> GetMembershipNo(long ID)
        {
            return this.dbContext.Fetch<Member>("where MemberNo  @0 or  MobileNo like @1 and Status = @2", "%" + ID, "%" + ID, "1");
          
        }

        public List<Member> GetMemberNoForMeeting(string ColumnName, long term)
        {
            return this.dbContext.Fetch<Member>("select  * from Members where " + ColumnName + " like @0 and Status = @1", "%"+ term + "%", "1");
                                                   
        }

        public int GetLastMemberID()
        {
            return this.dbContext.First<int>(" SELECT IFNULL(MAX(MemberNo), 0) FROM Members where Status = @0", "1");
        }

        public int GetInitialNo()
        {
            return this.dbContext.First<int>("SELECT * FROM initialnumbers"); 
        }

        public List<Member> MembersPrint(string Query, string PrintOption)
        {
            int value;
            if (int.TryParse(Query, out value))
            {
                // logger.Info("Inside Member Search Query for MemberShipNo = " + memberListViewModel.Query);
                return this.dbContext.Fetch<Member>("where (MemberNo = @0 or StorePincode = @0) AND Status = @1"
                    , Query
                    , "1");
            }
            else if (!string.IsNullOrEmpty(Query))
            {
                return this.dbContext.Fetch<Member>("where (MemberName like @0 OR MemberNo like @0 or Zone like @0 or MobileNo like @0 or StoreArea like @0 or MemberPost like @0)  AND Status = @1"
                   , "%" + Query + "%"
                        , "1");
            }
            else if (PrintOption.Equals("MembersDismissedList")) 
            {
                return this.dbContext.Fetch<Member>("Where (year(MembershipExpiryDate)=@0 and Status = @1) or Status=@2",  DateTime.Now.Year+1,"1","2");
            }
            else {
                return this.dbContext.Fetch<Member>("Where Status = @0", "1");
            }
        }

        public int GetMembersCount()
        {
            return this.dbContext.First<int>("select Count(MemberNo) from Members  where Status = @0", "1");
        }

        public int GetLifeMemberCount()
        {
            return this.dbContext.First<int>("select Count(MemberNo) from Members  where Status = @0 and MemberType = @1", "1","Life Member");
        }


        public List<Member> GetMembersExpiry()
        {
            return this.dbContext.Fetch<Member>("where Status = @0", "1");
        }

        public List<string> GetMemberAutoComplete(string ColumnName , string term)
        {
            return this.dbContext.Fetch<string>("select distinct " + ColumnName + "  from Members where " + ColumnName + " like @0 and Status = @1", term +"%", "1");
        }

        //  Check whether MemberNo avaliable in members table for New Member Inserting through Receipt.  
        public Member GetMemberForReceiptInsert(long memberID) {
            return this.dbContext.First<Member>("where MemberID =@0", memberID);
        }

        public List<Member> SendSms(string SmsTo, string Area)
        {
            if (SmsTo == "All")
            {
                return this.dbContext.Fetch<Member>("select MemberNo,MemberName,PhoneNo,Religion from Members");
            }
            else if (!string.IsNullOrEmpty(Area) && SmsTo == "Areawise")
            {
                return this.dbContext.Fetch<Member>("select MemberNo,MemberName,PhoneNo from Members where StoreArea =@0", Area);
            }
            else if (SmsTo != "MobileNumber")
            {
                return this.dbContext.Fetch<Member>("select MemberNo,MemberName,PhoneNo,Religion from Members where Religion =@0", SmsTo);
            }
            else {
                return null;
            }
        }

        public List<string> MembersAreaList(string term)
        {
            return this.dbContext.Fetch<string>("select distinct StoreArea  from Members where StoreArea like @0 and Status = @1", "%" + term + "%", "1");
        }

    }
}