﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Xlnc.Models;
using Xlnc.Utils;
using Xlnc.BALs;

namespace Xlnc.DALs
{
    public class MagazineDAL : AbstractDAL<Magazine>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceDAL));
        private MemberBAL memberBAL = new MemberBAL();
        public IEnumerable<Magazine> All()
        {
            return this.dbContext.Query<Magazine>("select * from Magazines  where Stauts =@0", "1");
        }

        public void Save(Magazine magazine)
        {
            try
            {
                magazine.Status = 1;
                this.dbContext.Save(magazine);
            }
            catch (Exception e) { 
                logger.Info("Error in Save Magazine " +e.Message);
            }

        }
        public void Update(Magazine magazine)
        {
            logger.Info("Update Magazine");
            magazine.Status = 1;
            this.dbContext.Update(magazine);
        }

        public int GetLastInsuranceNo() 
        {
            return this.dbContext.First<int>(" SELECT MAX(InsuranceNo) FROM Insurances");
        }

        public int Delete(long ID)
        {
           return this.dbContext.Update<Magazine>("Set Status = @0 Where MagazineID = @1","0",ID);
        }


        public Magazine FindByID(long ID)
        {
            return this.dbContext.First<Magazine>(" where MagazineID = @0", ID);
        }

        // Insurance Print For Single Member and Bulk 
        public List<Insurance> InsuranceReceiptListPrint(string Query,string FromDate, string ToDate)
        {
            if (!string.IsNullOrEmpty(Query))
            {
                return this.dbContext.Fetch<Insurance>("where (InsuranceNo like @0 OR MembershipNo like @1) and Status=@2 "
                   , "%" + Query + "%"
                        , "%" + Query + "%"
                        , "1");
            }
            else if (!string.IsNullOrEmpty(FromDate)) {
                return this.dbContext.Fetch<Insurance>("where Status=@0 and ReceiptDate between @1 and @2 "
                        , "1"
                        ,  FromDate
                        ,  ToDate);
            }
            else
            {
                return this.dbContext.Fetch<Insurance>("Where Status = @0", "1");
            }
        }

        // Dashboard Insurance Count
        public List<Insurance> GetInsurancesCount()
        {
            var Date = DateTime.Today;
            return this.dbContext.Fetch<Insurance>("where Status = @0 and ReceiptDate = @1", "1", Date);
            
        }


        // Dashboard Print Option
        public Insurance InsuranceReceiptSinglePrint(long InsuranceID, long MemberID)
        {
            Insurance insurance = new Insurance();
            if (InsuranceID > 0)
            {
                insurance = this.dbContext.First<Insurance>(" where InsuranceID like @0", InsuranceID);
            }
            else if (MemberID > 0)
            {
    
                var CurrentYear = DateTime.Now.Year.ToString();
                //CurrentYear + 1;
                logger.Info("CurrentYear = " + (CurrentYear));
                insurance = this.dbContext.First<Insurance>(" where MembershipNo like @0 ", MemberID);
            }
            return insurance;
        }

        public List<Magazine> AddressPrint(string Query, string PrintOption)
        {
            int value;
            if (int.TryParse(Query, out value))
            {
                // logger.Info("Inside Member Search Query for MemberShipNo = " + memberListViewModel.Query);
                return this.dbContext.Fetch<Magazine>("where (MobileNo = @0 or Pincode = @0 or MagazineID = @0) AND Status = @1"
                    , Query
                    , "1");
            }
            else if (!string.IsNullOrEmpty(Query))
            {
                return this.dbContext.Fetch<Magazine>("where (Name like @0 OR MemberID like @0 or MobileNo like @0 or Address like @0 or Area like @0 or City like @0)  AND Status = @1"
                   , "%" + Query + "%"
                        , "1");
            }
            else
            {
                return this.dbContext.Fetch<Magazine>("Where Status = @0", "1");
            }
        }
    }
}