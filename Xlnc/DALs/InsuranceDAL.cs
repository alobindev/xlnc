﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Xlnc.Models;
using Xlnc.Utils;
using Xlnc.BALs;
using PetaPoco;

namespace Xlnc.DALs
{
    public class InsuranceDAL : AbstractDAL<Insurance>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceDAL));
       // private MemberBAL memberBAL = new MemberBAL();
        public IEnumerable<Insurance> All()
        {
            return this.dbContext.Query<Insurance>("select * from Insurances  where Stauts =@0","1");
        }

        public void Save(Insurance insurance)
        {
            try
            {
                this.dbContext.Save(insurance);
               
                Member MemberDetails = new Member();
                try
                {
                    //logger.Info("Inside GetMemberForInsuranceInsert MembershipNO = " + insurance.MembershipNo);
                    //MemberDetails = this.memberBAL.GetMemberForReceiptInsert(insurance.MembershipNo);
                    //logger.Info("Checking whether member is available in members table MembershipID = " + MemberDetails.MembershipNo);
                    //if (MemberDetails.MembershipNo > 0)
                    //{

                    //    this.dbContext.Update<Member>("SET InsuranceAmount=@0 , InsurancePaidDate=@1 where MembershipNo=@2 ", insurance.PaidAmount, insurance.ReceiptDate, insurance.MembershipNo);
                    //}
                    ///*else
                    //{
                    //    logger.Info("Inside SaveMember in Receipts");
                    //    this.dbContext.Execute("Insert into Members (MembershipNo,MemberName,PhoneNo,StoreName,MemberShipAmountPaidDate,MembershipExpiryDate,MembershipAmount,MembershipForYear,Status) values(@0,@1,@2,@3,@4,@5,@6,@7,@8)", receipt.MembershipNo, receipt.MemberName, receipt.PhoneNo, receipt.StoreName, receipt.ReceiptDate, expirydate, receipt.PaidAmount, receipt.ReceiptFromYear + "-" + receipt.ReceiptToYear, "1");  //Insert("Members","MembershipNo = @0",receipt.MembershipNo);
                    //}*/
                }
                catch(Exception e) {
                    logger.Info("Error in GetMemberForInsurance" + e.Message);
                }
                
            }
            catch (Exception e) { 
                logger.Info("Error in Save Receipt " +e.Message);
            }

        }
        public void Update(Insurance insurance)
        {
            logger.Info("Update Insurance");

            this.dbContext.Update(insurance);
        //    this.dbContext.Update<Member>("SET InsuranceAmount=@0 , InsurancePaidDate=@1 where MembershipNo=@2 ", insurance.PaidAmount, insurance.ReceiptDate, insurance.MembershipNo);
        }

        public int GetLastInsuranceNo() 
        {
            return this.dbContext.First<int>(" SELECT MAX(InsuranceNo) FROM Insurances");
        }

        public void Delete(Insurance insurance)
        {
            insurance.Status = 0;
            this.dbContext.Update(insurance);
        }

        public Insurance FindByID(long insuranceID)
        {
            return this.dbContext.First<Insurance>(@"Select Insurances.*, Members.MemberNo, Members.MemberName, Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                                    Left Join Members on Members.MemberID = Insurances.MemberID  where InsuranceID = @0", insuranceID);
        }

        // Insurance Print For Single Member and Bulk 
        public List<Insurance> InsuranceReceiptListPrint(string Query,string FromDate, string ToDate)
        {
            if (!string.IsNullOrEmpty(Query))
            {
                var query = new Sql(@"Select Insurances.*, Members.MemberNo,Members.MemberName,Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                     Left Join Members On Members.MemberID = Insurances.MemberID
                                     where  InsuranceID like @0 or Members.MemberNo like @0 and Insurances.Status = @1", Query,"1");
                return this.dbContext.Fetch<Insurance>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate)) {
                var query = new Sql(@"Select Insurances.*, Members.MemberNo,Members.MemberName,Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                     Left Join Members On Members.MemberID = Insurances.MemberID
                                     where  (ReceiptDate between @0 and @1) and Insurances.Status = @2", "1", FromDate, ToDate);
                return this.dbContext.Fetch<Insurance>(query);
            }
            else
            {
                var query = new Sql(@"Select Insurances.*, Members.MemberNo,Members.MemberName,Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                     Left Join Members On Members.MemberID = Insurances.MemberID
                                     where  Insurances.Status = @0","1");
                return this.dbContext.Fetch<Insurance>(query);
            }
        }

        // Dashboard Insurance Count
        public List<Insurance> GetInsurancesCount()
        {
            var Date = DateTime.Today;
            return this.dbContext.Fetch<Insurance>("where Status = @0 and ReceiptDate = @1", "1", Date);
            
        }

        // Dashboard Print Option
        public Insurance InsuranceReceiptSinglePrint(long InsuranceID, long MemberID)
        {
            Insurance insurance = new Insurance();
            if (InsuranceID > 0)
            {
                var query = new Sql(@"Select Insurances.*, Members.MemberNo,Members.MemberName,Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                     Left Join Members On Members.MemberID = Insurances.MemberID
                                     where  InsuranceID = @0", InsuranceID);
                insurance = this.dbContext.Fetch<Insurance>(query).FirstOrDefault();
            }
            else if (MemberID > 0)
            {
    
                var CurrentYear = DateTime.Now.Year.ToString();
                //CurrentYear + 1;
                logger.Info("CurrentYear = " + (CurrentYear));
                insurance = this.dbContext.First<Insurance>(" where MemberNo like @0 ", MemberID);
            }
            return insurance;
        }
    }
}