﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Xlnc.Models;
using PetaPoco;

namespace Xlnc.DALs
{
    public class AccountDAL : AbstractDAL<User>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountDAL));
        public User AuthenticateUser(User user) {
            return this.dbContext.First<User>(" where Username=@0 and Password=@1",user.Username, user.Password);
        }

        public void Save(Account Account)
        {
            this.dbContext.Save(Account);
        }
        public void Update(Account Account)
        {
            logger.Info("Receipt ID in Update = "+Account.ReceiptID);
            //Account.Status = 1;
            this.dbContext.Update("Accounts", "AccountID", Account);
        }

        public void Delete(Account account)
        {
            //account.Status = 0;
            this.dbContext.Update<Account>("SET Status = @0 where AccountID=@1",0,account.AccountID);
        }
        public Account FindByID(long ID)
        {
            var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where AccountID = @0", ID);
            return this.dbContext.First<Account>(query);
        }

        public Account FindByIncomeBillNo(long ID)
        {
            var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where IncomeBillNo = @0", ID);
            return this.dbContext.First<Account>(query);
        }

        public Account FindByExpenseBillNo(long ID)
        {
            var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where ExpenseBillNo = @0", ID);
            return this.dbContext.First<Account>(query);
        }


        public List<Account> AccountsPrint(Sql query)
        {
            return this.dbContext.Fetch<Account>(query);
        }

        public List<Account> IncomeReceiptPrint(string Query, string FromDate,  string ToDate)
        {
            if (!string.IsNullOrEmpty(Query))
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        Where  (ExpenseBillNo like @0 OR Name like @0 or ReceiptDate like @0 or Amount like @0 or ChequeDate like @0 or ChequeNo like @0 OR Categorys.CategoryName like @0 or Reason like @0 or MobileNo like @0) 
                                        AND PaymentType = @1  AND Accounts.Status = @2 and Categorys.CategoryType Not In(@3) order BY ReceiptDate Desc"
                                       , "%" + Query + "%"
                                       , "Credit"
                                       , "1"
                                       , "Receipt");
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate))
            {

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status = @0 and PaymentType = @1 and Categorys.CategoryType Not In(@2) and ReceiptDate between @3 and @4"
                   , "1"
                   , "Credit"
                   , "Receipt"
                   , FromDate
                   , ToDate);

                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where Accounts.Status = @0 and PaymentType=@1 and Categorys.CategoryType Not In(@2) ORDER BY ReceiptDate DESC", "1", "Credit", "Receipt");
                return this.dbContext.Fetch<Account>(query);
            }
        }

        public List<Account> ExpenseReceiptPrint(string Query, string FromDate, string ToDate)
        {
            if (!string.IsNullOrEmpty(Query))
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        Where  (ExpenseBillNo like @0 OR Name like @0 or ReceiptDate like @0 or Amount like @0 or ChequeDate like @0 or ChequeNo like @0 OR Categorys.CategoryName like @0 or Reason like @0 or MobileNo like @0) 
                                        AND PaymentType = @1  AND Accounts.Status = @2 order BY ReceiptDate Desc"
                                       , "%" + Query + "%"
                                       , "Debit"
                                       , "1");
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate))
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and PaymentType = @1 and ReceiptDate between @2 and @3"
                                       , "1"
                                       , "Debit"
                                       , FromDate
                                       , ToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where Accounts.Status = @0 and PaymentType=@1 ORDER BY ReceiptDate DESC", "1", "Debit");
                return this.dbContext.Fetch<Account>(query);
            }
        }

        public int GetLastReceiptNo()
        {
            return this.dbContext.First<int>("SELECT IFNULL(MAX(AccountNo), 0) FROM Accounts"); //(" SELECT MAX(AccountNo) FROM Accounts");
        }

        public List<Account> GetIncomeAndExpenseDetails(DateTime FromDate, DateTime ToDate)
        {
            if (FromDate != null)
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and ReceiptDate between @2 and @3"
                                      , "1"
                                      , FromDate
                                      , ToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status =@0 and ReceiptDate =@1", "1", DateTime.Now);
                return this.dbContext.Fetch<Account>(query);
            }
        }


        public List<Account> IncomeAndExpenseDetailsPrint(string FromDate, string ToDate)
        {
            if (FromDate != null)
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and ReceiptDate between @2 and @3"
                                      , "1"
                                      , FromDate
                                      , ToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status =@0 and ReceiptDate =@1", "1", DateTime.Now);
                return this.dbContext.Fetch<Account>(query);
            }
        }

        public List<Account> GetGroupByIncomeAndExpenseDetailsPrint(string FromDate, string ToDate)
        {
            if (FromDate != null)
            {
                var query = new Sql(@"SELECT Sum(Amount) as Amount,Accounts.CategoryID,Accounts.ReceiptDate,Accounts.Reason,Categorys.CategoryName,PaymentType FROM `accounts`
                                        Left Join Categorys on Categorys.CategoryID = Accounts.CategoryID Where Accounts.Status = @0 and (ReceiptDate between @1 and @2) GROUP BY Accounts.CategoryID"
                                      , "1"
                                      , FromDate
                                      , ToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"SELECT Sum(Amount) as Amount,Accounts.CategoryID,Accounts.ReceiptDate,Accounts.Reason,Categorys.CategoryName,PaymentType FROM `accounts`
                                        Left Join Categorys on Categorys.CategoryID = Accounts.CategoryID Where Accounts.Status = @0 and (ReceiptDate = @1) GROUP BY Accounts.CategoryID", "1", DateTime.Now);
                return this.dbContext.Fetch<Account>(query);
            }
        }


    }
}