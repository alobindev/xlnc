﻿using Xlnc.Controllers;
using Xlnc.DALs;
using Xlnc.Models;
using PetaPoco;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Xlnc.BALs
{
    public class MeetingBAL
    {
        private MeetingDAL meetingDAL = new MeetingDAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MeetingController));
        

        public void Save(Meeting meeting)
        {
            try
            {
                this.meetingDAL.Save(meeting);
            }
            catch (Exception e)
            {
                logger.Info("Error Message in Meetin BAL" + e.Message);
            }
        }
        public Page<Meeting> PagedQuery(MeetingListViewModel meetingListViewModel)
        {
            try
            {
                var MeetingList = this.meetingDAL.PagedQuery<Meeting>(meetingListViewModel.PageNumber, meetingListViewModel.ItemsPerPage, "Select * from meetings where status = @0",1);
                return MeetingList;
            }
            catch (Exception e)
            {
                logger.Info("Error message in meetinglist BAL" + e.Message);
            }
            return null;
        }

        public Page<MemberEntry> PagedQuery(MemberEntryViewModel memberEntryViewModel)
        {
            try
            {
                var query = new Sql(@"select MemberEntries.*,Members.MemberName,Members.MemberNo,Members.StoreName,Members.MobileNo from MemberEntries
                                    Left Join Members on Members.MemberID = MemberEntries.MemberID
                                    where MemberEntries.status = @0 AND MemberEntries.MeetingID = @1 "
                                    , 1, memberEntryViewModel.MeetingID);
                var MeetingLists = this.meetingDAL.PagedQuery<MemberEntry>(memberEntryViewModel.PageNumber, memberEntryViewModel.ItemsPerPage, query);
                return MeetingLists;
            }
            catch (Exception e)
            {
                logger.Info("Error message in meetinglist BAL" + e.Message);
            }
            return null;
        }
         public void Update(Meeting meeting)
         {
             try
             {
                 this.meetingDAL.Update(meeting);
             }
             catch(Exception e)
             {
                 logger.Info("Error Message in Meetin BAL" + e);
             }
         }
         
        public bool SaveMemberEntry(MemberEntry memberEntry)
        {
           
            bool checkmemberentrys = this.meetingDAL.getMemberExists(memberEntry);
           
            if (checkmemberentrys)
             {
               // var Message = "Member Exists";
                return false;
                

            }
            else
             {
                this.meetingDAL.SaveMemberEntry(memberEntry);
                return true;
             }
            
        }
         
        public Meeting FindByID(long MeetingID)
        {
            Meeting meetingdetails = new Meeting();
            try
            {
                logger.Info("Inside FindByID MeetingID = " + MeetingID);
                meetingdetails = this.meetingDAL.FindByID(MeetingID);
            }
            catch (Exception e)
            {
                logger.Info("Error in FindByID meeting BAL " + e.Message);
            }
            return meetingdetails;
        }

        public int Delete(long ID)
        {
            int deletemeeting = 0;
            try
            {
                deletemeeting = this.meetingDAL.Delete(ID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Member Delete" + e.Message);
            }
            return deletemeeting;
        }
        public List<MemberEntry> MembersPrint(string MeetingID)
        {
            var members = this.meetingDAL.MembersPrint(MeetingID);
            return members;
        }
       /* public Meeting GetMeeting(long meetingID)
        {
            return this.meetingDAL.GetMeeting(meetingID);
        }*/
    }
}