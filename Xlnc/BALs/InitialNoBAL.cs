﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.Models;
using Xlnc.DALs;
using PetaPoco;
using Xlnc.Utils;

namespace Xlnc.BALs
{
    public class InitialNoBAL
    {
        private InitialNoDAL initialnoDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InitialNumber));

        public InitialNoBAL() {

            try
            {
                this.initialnoDAL = new InitialNoDAL();
            }
            catch(Exception e) { 
            }
        }
        public InitialNumber GetInitialNo(string ResourceType)
        {
            InitialNumber initialno = new InitialNumber();
            logger.Info("Inside GetInitialNo ResourceType= " + ResourceType);
            try
            /*{
                initialno.Prefix = "LX";
                initialno.LastNo = 0;
                var newlastno = initialno.LastNo + 1;

            }*/
            {
                initialno = this.initialnoDAL.GetInitialNo(ResourceType);
                logger.Info("GetInitialNo ResourceType= " + ResourceType.ToString());
                if (initialno != null) { 
                    long update = initialno.LastNo ;
                    logger.Info("InitialNo Value = " + update);
                    long updatedvalue = update + 1;
                    logger.Info("Updated InitialNo =" + updatedvalue);
                    this.initialnoDAL.UpdateInitialNo(Convert.ToInt32(updatedvalue), ResourceType);
                }
            }

            catch (Exception e)
            {
                logger.Info("Error in GetInitialNo =" + e.Message);
            }
            return initialno;
        }
        
    }    
}