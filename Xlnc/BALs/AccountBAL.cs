﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.Models;
using Xlnc.DALs;
using PetaPoco;
using Xlnc.Utils;

namespace Xlnc.BALs
{
    public class AccountBAL
    {
        private AccountDAL accountDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountBAL));
        public AccountBAL() {

            try
            {
                this.accountDAL = new AccountDAL();
            }
            catch(Exception e) { 
            }
        }


        public void Save(Account Account)
        {
            try
            {
                int getlastreceiptno = 0;
                try
                {
                    getlastreceiptno = this.accountDAL.GetLastReceiptNo();
                }
                catch (Exception e)
                {

                    logger.Info("No Records Found " + e.Message);
                }
                Account.AccountNo = getlastreceiptno + 1;
                this.accountDAL.Save(Account);
            }
            catch (Exception e)
            {
                logger.Info("Error in Save DailyIncome " + e.Message);
            }
        }
        public void Update(Account Account)
        {
            try
            {
                logger.Info("Receipt ID in BAL= " + Account.ReceiptID);
                this.accountDAL.Update(Account);
            }
            catch (Exception e)
            {
                logger.Info("Error in Update DailyIncome" + e);
            }
        }

        public Sql GenerateQuery(AccountListViewModel accountListViewModel)
        {
            var accountFilter = accountListViewModel.AccountFilter;
            string billNoColumnName = "";
            if (accountFilter.AccountType.Equals("Credit"))
                billNoColumnName = "IncomeBillNo";
            else
                billNoColumnName = "ExpenseBillNo";
            var query = new Sql("Select Accounts.*, Categorys.CategoryName from Accounts");
            query.LeftJoin("Categorys").On("Categorys.CategoryID = Accounts.CategoryID");

            int value;
            if ((int.TryParse(accountListViewModel.Query, out value)) && ((accountListViewModel.Query).Length <= 6))
            {
                query.Where("" + billNoColumnName + " = @0 or Accounts.Amount = @0", accountListViewModel.Query);
            }
            if ((!string.IsNullOrEmpty(accountListViewModel.Query)))
            {
                query.Where("" + billNoColumnName + " like @0 OR Accounts.Name like @0 or Accounts.ReceiptDate like @0 or Accounts.Amount like @0 or Accounts.ChequeDate like @0 or Accounts.ChequeNo like @0 OR Categorys.CategoryName like @0 or Accounts.Reason like @0 or Accounts.MobileNo like @0"
                    , "%" + accountListViewModel.Query + "%");
            }
            if (accountFilter != null & accountFilter.FromDateTime != null && accountFilter.ToDateTime != null)
            {
                query.Where("Accounts.ReceiptDate between @0 and @1"
                    , accountFilter.FromDateTime
                    , accountFilter.ToDateTime);
            }
            else if (accountFilter != null & accountFilter.FromDateTime != null)
            {
                query.Where("Accounts.ReceiptDate >= @0", accountFilter.FromDateTime);
            }
            else if (accountFilter != null & accountFilter.FromDateTime != null)
            {
                query.Where("Accounts.ReceiptDate <= @0", accountFilter.ToDateTime);
            }
            query.Where("Accounts.PaymentType = @0", accountFilter.AccountType);
            query.Where("Accounts.Status = @0", 1);
            query.Where("Categorys.CategoryType Not In(@0)", "Receipt");
            query.OrderBy("Accounts.ReceiptDate DESC");
            return query;
        }

        public Page<Account> PagedQuery(AccountListViewModel accountListViewModel)
        {
            try
            {
                var query = this.GenerateQuery(accountListViewModel);
                return this.accountDAL.PagedQuery<Account>(accountListViewModel.PageNumber, accountListViewModel.ItemsPerPage, query);
            }
            catch (Exception e)
            {
                logger.Info("Error in Paged Query " + e.Message);
            }
            return null;
        }

        public Page<Account> IncomePagedQuery(AccountListViewModel incomeListViewModel)
        {
            try
            {
                // Todo: change this task of sql to DAL
                if ((!string.IsNullOrEmpty(incomeListViewModel.Query)))
                {
                    logger.Info("Inside Income Search Query = " + incomeListViewModel.Query);
                    var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        Where  (IncomeBillNo like @0 OR Name like @0 or ReceiptDate like @0 or Amount like @0 or ChequeDate like @0 or ChequeNo like @0 OR Categorys.CategoryName like @0 or Reason like @0 or MobileNo like @0) 
                                        AND PaymentType = @1  AND Accounts.Status = @2 and Categorys.CategoryType Not In(@3) order BY ReceiptDate Desc"
                                        , "%" + incomeListViewModel.Query + "%"
                                        , "Credit"
                                        , "1"
                                        , "Receipt");
                    var incomes = this.accountDAL.PagedQuery<Account>(incomeListViewModel.PageNumber, incomeListViewModel.ItemsPerPage, query);
                    return incomes;
                }
                else
                {
                    var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where Accounts.Status = @0 and PaymentType=@1 and Categorys.CategoryType Not In(@2) ORDER BY ReceiptDate DESC", "1", "Credit", "Receipt");
                    return this.accountDAL.PagedQuery<Account>(incomeListViewModel.PageNumber, incomeListViewModel.ItemsPerPage, query);
                }

            }
            catch (Exception e)
            {
                logger.Info("Error in Paged Query " + e.Message);
            }
            return null;
        }

        public Page<Account> ExpensePagedQuery(AccountListViewModel incomeListViewModel)
        {
            try
            {
                // Todo: change this task of sql to DAL
                if ((!string.IsNullOrEmpty(incomeListViewModel.Query)))
                {
                    logger.Info("Inside Income Search Query = " + incomeListViewModel.Query);
                    var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        Where  (ExpenseBillNo like @0 OR Name like @0 or ReceiptDate like @0 or Amount like @0 or ChequeDate like @0 or ChequeNo like @0 OR Categorys.CategoryName like @0 or Reason like @0 or MobileNo like @0) 
                                        AND PaymentType = @1  AND Accounts.Status = @2 order BY ReceiptDate Desc"
                                         , "%" + incomeListViewModel.Query + "%"
                                         , "Debit"
                                         , "1");

                    var incomes = this.accountDAL.PagedQuery<Account>(incomeListViewModel.PageNumber, incomeListViewModel.ItemsPerPage, query);
                    return incomes;
                }
                else if (!string.IsNullOrEmpty(incomeListViewModel.FromDate))
                {
                    logger.Info("Inside Members Search Query = " + incomeListViewModel.FromDate + "and" + incomeListViewModel.ToDate);
                    var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and PaymentType = @1 and ReceiptDate between @2 and @3"
                                        , "1"
                                        , "Debit"
                                        , incomeListViewModel.FromDate
                                        , incomeListViewModel.ToDate);

                    var incomes = this.accountDAL.PagedQuery<Account>(incomeListViewModel.PageNumber, incomeListViewModel.ItemsPerPage, query);
                    return incomes;
                }
                else
                {
                    var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where Accounts.Status = @0 and PaymentType=@1 ORDER BY ReceiptDate DESC", "1", "Debit");
                    return this.accountDAL.PagedQuery<Account>(incomeListViewModel.PageNumber, incomeListViewModel.ItemsPerPage, query);
                }

            }
            catch (Exception e)
            {
                logger.Info("Error in Paged Query " + e.Message);
            }
            return null;
        }

        public Account FindByID(long AccountID)
        {
            Account voucherdetails = new Account();
            try
            {
                logger.Info("Inside FindByID ReceiptID = " + AccountID);
                voucherdetails = this.accountDAL.FindByID(AccountID);
            }
            catch (Exception e)
            {
                logger.Info("Error in FindByID " + e.Message);
            }
            return voucherdetails;
        }

        public Account FindByIncomeBillNo(long BillNo)
        {
            Account voucherdetails = new Account();
            try
            {
                logger.Info("Inside FindByBillNo BillNo = " + BillNo);
                voucherdetails = this.accountDAL.FindByIncomeBillNo(BillNo);
            }
            catch (Exception e)
            {
                logger.Info("Error in FindByBillNo " + e.Message);
            }
            return voucherdetails;
        }
        public Account FindByExpenseBillNo(long BillNo)
        {
            Account voucherdetails = new Account();
            try
            {
                logger.Info("Inside FindByBillNo BillNo = " + BillNo);
                voucherdetails = this.accountDAL.FindByExpenseBillNo(BillNo);
            }
            catch (Exception e)
            {
                logger.Info("Error in FindByBillNo " + e.Message);
            }
            return voucherdetails;
        }
        public void Delete(Account account)
        {
             this.accountDAL.Delete(account);
        }


        public List<Account> AccountsPrint(AccountListViewModel accountListViewModel)
        {
            List<Account> incomereceiptdetails = new List<Account>();
            try
            {
                var query = this.GenerateQuery(accountListViewModel);
                incomereceiptdetails = this.accountDAL.AccountsPrint(query);
            }
            catch (Exception e)
            {
                logger.Info("Error in Accounts Print" + e.Message);
            }
            return (incomereceiptdetails);
        }
        public List<Account> IncomeReceiptPrint(string Query, string FromDate, string ToDate)
        {
            List<Account> incomereceiptdetails = new List<Account>();
            try
            {
                logger.Info("Inside Membersprint " + Query);
                incomereceiptdetails = this.accountDAL.IncomeReceiptPrint(Query,FromDate,ToDate);
            }
            catch (Exception e)
            {
                logger.Info("Error in MembersPrint" + e.Message);
            }
            return (incomereceiptdetails);
        }

        public List<Account> ExpenseReceiptPrint(string Query, string FromDate, string ToDate)
        {
            List<Account> expensereceiptdetails = new List<Account>();
            try
            {
                logger.Info("Inside ExpenseReceiptPrint Membersprint " + Query);
                expensereceiptdetails = this.accountDAL.ExpenseReceiptPrint(Query, FromDate, ToDate);
            }
            catch (Exception e)
            {
                logger.Info("Error in ExpenseReceiptPrint MembersPrint" + e.Message);
            }
            return (expensereceiptdetails);
        }

        public Page<Account> GetIncomeAndExpenseDetails(AccountListViewModel accountListViewModel)
        {
            try
            {
                // Todo: change this task of sql to DAL
                if ((!string.IsNullOrEmpty(accountListViewModel.FromDate)) && (!string.IsNullOrEmpty(accountListViewModel.ToDate)))
                {
                    DateTime MyFromDate = DateTime.ParseExact(accountListViewModel.FromDate, "dd/MM/yyyy", null);
                    DateTime MyToDate = DateTime.ParseExact(accountListViewModel.ToDate, "dd/MM/yyyy", null);
                    
                    string FromDate = MyFromDate.ToString("yyyy-MM-dd");
                    string ToDate = MyToDate.ToString("yyyy-MM-dd");
                    
                    var accounts = this.accountDAL.PagedQuery<Account>(accountListViewModel.PageNumber, accountListViewModel.ItemsPerPage
                        , "where ReceiptDate between @0 and @1 AND Status = @2"
                        , FromDate , ToDate,"1");
                    return accounts;
                }
                else
                    return this.accountDAL.PagedQuery<Account>(accountListViewModel.PageNumber, accountListViewModel.ItemsPerPage, "Where Status = @0 and ReceiptDate=@1", "1",DateTime.Now.Date);
            }
            catch (Exception e)
            {
                logger.Info("Error in Paged Query " + e.Message);
            }
            return null;
        }

        public List<Account> IncomeAndExpenseDetailsPrint(string FromDate, string ToDate)
        {
            List<Account> incomeandexpenseprint = new List<Account>();
            DateTime MyFromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
            DateTime MyToDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

            string NewFromDate = MyFromDate.ToString("yyyy-MM-dd");
            string NewToDate = MyToDate.ToString("yyyy-MM-dd");
            try
            {
                logger.Info("Inside IncomeAndExpenseDetailsPrint " + NewFromDate);
                incomeandexpenseprint = this.accountDAL.IncomeAndExpenseDetailsPrint(NewFromDate, NewToDate);
            }
            catch (Exception e)
            {
                logger.Info("Error in IncomeAndExpenseDetailsPrint" + e.Message);
            }
            return (incomeandexpenseprint);
        }

        public Page<Account> GetGroupByIncomeAndExpenseDetails(AccountListViewModel accountListViewModel)
        {
            try
            {
                // Todo: change this task of sql to DAL
                if ((!string.IsNullOrEmpty(accountListViewModel.FromDate)) && (!string.IsNullOrEmpty(accountListViewModel.ToDate)))
                {
                    DateTime MyFromDate = DateTime.ParseExact(accountListViewModel.FromDate, "dd/MM/yyyy", null);
                    DateTime MyToDate = DateTime.ParseExact(accountListViewModel.ToDate, "dd/MM/yyyy", null);

                    string FromDate = MyFromDate.ToString("yyyy-MM-dd");
                    string ToDate = MyToDate.ToString("yyyy-MM-dd");

                    var query = new Sql(@"SELECT Sum(Amount) as Amount,Accounts.CategoryID,Accounts.ReceiptDate,Accounts.Reason,Categorys.CategoryName,PaymentType FROM `accounts`
                                        Left Join Categorys on Categorys.CategoryID = Accounts.CategoryID Where Accounts.Status = @0 and (ReceiptDate between @1 and @2) GROUP BY Accounts.CategoryID ", 1, FromDate, ToDate);
                    var accounts = this.accountDAL.PagedQuery<Account>(accountListViewModel.PageNumber, accountListViewModel.ItemsPerPage, query);
                    return accounts;
                }
                else
                {
                    var query = new Sql(@"SELECT Sum(Amount) as Amount,Accounts.CategoryID,Accounts.ReceiptDate,Accounts.Reason,Categorys.CategoryName,PaymentType FROM `accounts`
                                        Left Join Categorys on Categorys.CategoryID = Accounts.CategoryID Where Accounts.Status = @0 and (ReceiptDate = @1) GROUP BY Accounts.CategoryID ", 1, DateTime.Now.Date);
                    return this.accountDAL.PagedQuery<Account>(accountListViewModel.PageNumber, accountListViewModel.ItemsPerPage, query);
                }
            }
            catch (Exception e)
            {
                logger.Info("Error in Paged Query " + e.Message);
            }
            return null;
        }

    }
}