﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.Models;
using Xlnc.DALs;
using Xlnc.Utils;

namespace Xlnc.BALs
{
    public class RoomBAL
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RoomBAL));
        private RoomDAL roomDAL = new RoomDAL();

        public List<Room> GetRooms()
        {
            return this.roomDAL.GetRooms();
        }

        public List<Room> GetAvailableRooms(string filter)
        {
            return this.roomDAL.GetAvailableRooms(filter);
        }

        public List<Room> GetAllRooms(string filter)
        {
            return this.roomDAL.GetAllRooms(filter);
        }

        public List<Room> GetOccupiedRooms(string filter)
        {
            return this.roomDAL.GetOccupiedRooms(filter);
        }

        public void RoomTransfer(RoomTransferEditViewModel roomTransferEditModel)
        {
            try
            {
                logger.Info("Trying to TransferRoom, data = " + Newtonsoft.Json.JsonConvert.SerializeObject(roomTransferEditModel));
                this.roomDAL.RoomTransfer(roomTransferEditModel.RoomTransfer);
                Auth.SetStatusMessage("Room Transferred Successfully");
            }
            catch (Exception ex)
            {
                logger.Error("Problem in Room Transfer, due to" + ex);
                Auth.SetStatusMessage("Problem in trnasferring room, please try again.", "Warning");
            }
        }
    }
}