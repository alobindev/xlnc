﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.Models;
using Xlnc.DALs;
using PetaPoco;
using log4net;
using Xlnc.Utils;
namespace Xlnc.BALs
{
    public class InsuranceBAL
    {
        private InsuranceDAL insuranceDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceBAL));
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        public InsuranceBAL()
        {
            try
            {
                this.insuranceDAL = new InsuranceDAL();
            }
            catch (Exception e)
            {
            }
        }

        public IEnumerable<Insurance> All()
        {
            return new List<Insurance>()
            {
                new Insurance() {
                }
            };
        }

        public Page<Insurance> PagedQuery(InsuranceListViewModel receiptListViewModel)
        {
            logger.Info("Insurance Page Query");
           
            try {
                if ((!string.IsNullOrEmpty(receiptListViewModel.Query)))
                {
                    logger.Info("Insurance Search Query" + receiptListViewModel.Query);
                    var query = new Sql(@"Select Insurances.*, Members.MemberNo, Members.MemberName, Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                        Left Join Members on Members.MemberID = Insurances.MemberID
                                        Where PolicyNo like @0 or Members.MemberNo like @1 and Insurances.Status = @2"
                        , "%" + receiptListViewModel.Query + "%"
                        , "%" + receiptListViewModel.Query + "%"
                        , "1");
                    var receipts = this.insuranceDAL.PagedQuery<Insurance>(receiptListViewModel.PageNumber, receiptListViewModel.ItemsPerPage, query);
                    return receipts;
                }
                else if (!string.IsNullOrEmpty(receiptListViewModel.FromDate))
                {
                    logger.Info("Inside Insurance Search Query = " + receiptListViewModel.FromDate + "and" + receiptListViewModel.ToDate);
                    var query = new Sql(@"Select Insurances.*, Members.MemberNo, Members.MemberName, Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                        Left Join Members on Members.MemberID = Insurances.MemberID
                                        Where (ReceiptDate between @0 and @1) and Insurances.Status = @2"
                  , receiptListViewModel.FromDate
                  , receiptListViewModel.ToDate
                  , "1");
                    var receipts = this.insuranceDAL.PagedQuery<Insurance>(receiptListViewModel.PageNumber, receiptListViewModel.ItemsPerPage, query);
                    return receipts;
                }
                else
                {
                    var query = new Sql(@"Select Insurances.*, Members.MemberNo, Members.MemberName, Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                        Left Join Members on Members.MemberID = Insurances.MemberID
                                        Where Insurances.Status = @0", "1");
                    return this.insuranceDAL.PagedQuery<Insurance>(receiptListViewModel.PageNumber, receiptListViewModel.ItemsPerPage, query);
                }
            }
            
            catch (Exception e)
            {
                logger.Info("Error in Insurance Search" + e.Message);
            }
            return null;
        }

        public void Save(Insurance insurance)
        {
            try
            {
                int getlastreceiptno = 0;
                try
                {
                 //   getlastreceiptno = this.insuranceDAL.GetLastInsuranceNo();
                }
                catch (Exception e){
                    logger.Info("No Records Found"+e.Message);
                }
                logger.Info("Inside SaveInsurance BAL " + insurance.ToString());
                logger.Info("Inside GetLastInsuranceNo BAL " + getlastreceiptno);
               // insurance.InsuranceID = getlastreceiptno + 1;
                this.insuranceDAL.Save(insurance);
            }
            catch (Exception e)
            {
                logger.Info("Error in SaveInsurance BAL" + e.Message);
            }
        }

        public void Update(Insurance insurance) {
            try
            {
                logger.Info("Inside Update Insurance BAL " + insurance.ToString());
                this.insuranceDAL.Update(insurance);
            }
            catch (Exception e){
                logger.Info("Error in Update Insurance BAL " + e.Message);
            }
        }

        public void Delete(Insurance insurance)
        {
            try
            {
                this.insuranceDAL.Delete(insurance);
            }
            catch (Exception e)
            {
            }
        }

        public Insurance FindByID(long receiptID)
        {
            try
            {
                logger.Info("Inside Insurance FindByID = " + receiptID);
                  return this.insuranceDAL.FindByID(receiptID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Insurance FindByID =" + e.Message);
            }
            return null;
        }
        public List<Insurance> InsuranceReceiptListPrint(string Query,string FromDate, string ToDate)
        {
            List<Insurance> insurancedetails = new List<Insurance>();
            try
            {
                logger.Info("Inside Insuranceprint " + Query);
                insurancedetails = this.insuranceDAL.InsuranceReceiptListPrint(Query,FromDate,ToDate);
            }
            catch (Exception e)
            {
                logger.Info("Error in InsurancePrint" + e.Message);
            }
            return (insurancedetails);
        }

        public List<Insurance> GetInsurancesCount()
        {
            List<Insurance> insurancecount = new List<Insurance>();
            try
            {
                logger.Info("Inside GetInsuranceCount " );
                insurancecount = this.insuranceDAL.GetInsurancesCount();
            }
            catch (Exception e)
            {
                logger.Info("Error in GetInsuranceCount" + e.Message);
            }
            return (insurancecount);
        }
        public Insurance InsuranceReceiptSinglePrint(long InsuranceID, long MemberID)
        {
            try
            {
                logger.Info("Inside Insurance InsuranceReceiptSinglePrint = " + InsuranceID + "& MemberID = " + MemberID);
                return this.insuranceDAL.InsuranceReceiptSinglePrint(InsuranceID, MemberID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Insurance InsuranceSinglePrint =" + e.Message);
            }
            return null;
        }

        public void SaveMember(Insurance receipt) 
        {
            try {
                logger.Info("Inside SaveMember");
              //  this.receiptDAL.SaveMember(receipt);
            }
            catch (Exception e) {
                logger.Info("Error in SaveMember " + e.Message);
            }
        }
    }
}