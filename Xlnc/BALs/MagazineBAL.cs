﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.Models;
using Xlnc.DALs;
using PetaPoco;
using log4net;
using Xlnc.Utils;
namespace Xlnc.BALs
{
    public class MagazineBAL
    {
        private MagazineDAL magazineDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceBAL));
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        public MagazineBAL()
        {
            try
            {
                this.magazineDAL = new MagazineDAL();
            }
            catch (Exception e)
            {
            }
        }

        public IEnumerable<Magazine> All()
        {
            return new List<Magazine>()
            {
                new Magazine() {
                }
            };
        }

        public Page<Magazine> PagedQuery(MagazineListViewModel magazineListViewModel)
        {
            logger.Info("Magazine Page Query");
           
            try {
                if ((!string.IsNullOrEmpty(magazineListViewModel.Query)))
                {
                    logger.Info("Magazine Search Query" + magazineListViewModel.Query);
                    var query = new Sql(@"Select * from Magazines Where Name like @0 and Status = @1"
                        , "%" + magazineListViewModel.Query + "%"
                        , "1");
                    var magazines = this.magazineDAL.PagedQuery<Magazine>(magazineListViewModel.PageNumber, magazineListViewModel.ItemsPerPage, query);
                    return magazines;
                }
                else
                {
                    var query = new Sql(@"Select * from Magazines Where Status = @0"
                       , "1");
                    return this.magazineDAL.PagedQuery<Magazine>(magazineListViewModel.PageNumber, magazineListViewModel.ItemsPerPage, query);
                }
            }
            
            catch (Exception e)
            {
                logger.Info("Error in Insurance Search" + e.Message);
            }
            return null;
        }

        public void Save(Magazine magazine)
        {
            try
            {
                this.magazineDAL.Save(magazine);
            }
            catch (Exception e)
            {
                logger.Info("Error in Save Magazine BAL" + e.Message);
            }
        }

        public void Update(Magazine magazine)
        {
            try
            {
                logger.Info("Inside Update magazine BAL " + magazine.ToString());
                this.magazineDAL.Update(magazine);
            }
            catch (Exception e)
            {
                logger.Info("Error in Update magazine BAL " + e.Message);
            }
        }

        public int Delete(long ID)
        {
            try
            {
              return  this.magazineDAL.Delete(ID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Delete Magazine " +e.Message);
            }
            return 0;
        }

        public Magazine FindByID(long magazineID)
        {
            try
            {
                logger.Info("Inside Insurance FindByID = " + magazineID);
                return this.magazineDAL.FindByID(magazineID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Insurance FindByID =" + e.Message);
            }
            return null;
        }

        public List<Magazine> AddressPrint(string Query, string PrintOption)
        {
            List<Magazine> memberdetails = new List<Magazine>();
            try
            {
                logger.Info("Inside MagazineAddressPrint =" + Query + " PrintOption = " + PrintOption);
                memberdetails = this.magazineDAL.AddressPrint(Query, PrintOption);
            }
            catch (Exception e)
            {
                logger.Info("Error in MagazineAddressPrint" + e.Message);
            }
            return (memberdetails);
        }
    }
}