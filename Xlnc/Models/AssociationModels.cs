﻿using System;
using PetaPoco;
using System.IO;
using System.Collections.Generic;
using System.Web;

namespace Xlnc.Models
{
    [TableName("Members")]
    [PrimaryKey("MemberID")]
    public class Member
    {
        public long MemberID { get; set; }
        public long MemberNo { get; set; }
        public string MemberName { get; set; }
        public string PhotoName { get; set; }
        [Ignore]
        public HttpPostedFileBase FileData { get; set; }

        public DateTime? DateOfBirth { get; set; }
        [Ignore]
        public string DateOfBirthStr { get; set; }
        public int Age { get; set; }
        public string FatherName { get; set; }
        public string SpouseName { get; set; }
        public string Children1Name { get; set; }
        public DateTime Child1DateOfBirth { get; set; }
        public string Children2Name { get; set; }
        public DateTime Child2DateOfBirth { get; set; }
        public string NomineeName { get; set; }
        public string Relationship { get; set; }
        public string StoreName { get; set; }
        public string MobileNo { get; set; }
        public string TelephoneNo { get; set; }
        public string StoreStartingYear { get; set; }
        public int TotalExperience { get; set; }
        public string BloodGroup { get; set; }
        //public int MalesCount { get; set; }
        //public int FemalesCount { get; set; }
        //public int StaffsCount { get; set; }
        // public string CategoryName { get; set; }
        public string Qualification { get; set; }
        public string SpecialistIn { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public double MembershipAmount { get; set; }
        public DateTime MembershipExpiryDate { get; set; }
        public DateTime? JoiningDate { get; set; }
        [Ignore]
        public string JoiningDateStr { get; set; }
        public string MemberPost { get; set; }
        public string StoreAddress { get; set; }
        public string StoreSubArea { get; set; }
        public string StoreArea { get; set; }
        public string StoreCity { get; set; }
        public long StorePincode { get; set; }
        public string AadhaarNumber { get; set; }
        public string DrivingLicenceNo { get; set; }
        public DateTime LicenceValidDate { get; set; }
        public string HomeAddress { get; set; }
        public string HomeSubArea { get; set; }
        public string HomeArea { get; set; }
        public string HomeCity { get; set; }
        public long HomePincode { get; set; }
        public string Community { get; set; }
      //  public double PensionAmount { get; set; }
        public double InsuranceAmount { get; set; }
        public DateTime? InsurancePaidDate { get; set; }
        public string Email { get; set; }
        public string MemberSignatureUrl { get; set; }
        public int Status { get; set; }
        public DateTime DismissDate { get; set; }
        public string DismissReason { get; set; }
        public string MembershipForYear { get; set; }
        public string Religion { get; set; }
        public string MembershipPaid { get; set; }
        public DateTime WeddingDate { get; set; }
        public string Reference { get; set; }
        public long MemberCategoryID { get; set; }
        public string Validity { get; set; }
        public string OwnerShipType { get; set; }
        public string MaritalStatus { get; set; }
        public string EmergencyNo { get; set; }
        public long CategoryID { get; set; }
        public int RoomNo { get; set; }
        public int LockerNo { get; set; }
        // public string Zonal { get; set; }

    }
    [TableName("MembershipDetails")]
    [PrimaryKey("MembershipDetailsID")]
    public class MembershipDetails {
        public long MembershipDetailsID { get; set; }
        public long MemberID { get; set; }
        public double MembershipAmount { get; set; }
        public DateTime MembershipPaidDate { get; set; }
        public string MembershipForYear { get; set; }
        public DateTime MembershipValidity { get; set; }
        public int MembershipPaidStatus { get; set; }
    }
    
    [TableName("Receipts")]
    [PrimaryKey("ReceiptID")]
    public class Receipt
    {
        public long ReceiptID { get; set; }
        public long ReceiptNo { get; set; }
        public long ReceiptBillNo { get; set; }
        public long MemberID { get; set; }
        public int RoomNo { get; set; }
        public int LockerNo { get; set; }
        public double PaidAmount { get; set; }
        public string PaymentMode { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
        public DateTime? ReceiptDate { get; set; }
        public string ReceiptForMonth { get; set; }
        public int Status { get; set; }
        public long CategoryID { get; set; }  
        public string Description { get; set; }
        public string TransactionType { get; set; }
        [ResultColumn]
        public string MemberName { get; set; }
        [ResultColumn]
        public string CategoryName { get; set; }
        
    }

    [TableName("Rooms")]
    [PrimaryKey("RoomID")]
    public class Room
    {
        public int RoomID { get; set; }
        public int RoomNo { get; set; }
        public int LockerNo { get; set; }
        public string RoomSharingType { get; set; }
        public int IsBooked { get; set; }
        public long MemberID { get; set; }
    }

    [TableName("Bookings")]
    [PrimaryKey("BookingID")]
    public class Booking
    {
        public long BookingID { get; set; }
        public long MemberNo { get; set; }
        public int RoomNo { get; set; }
        public int LockerNo { get; set; }
        public DateTime BookedDate { get; set; }
        public int UserID { get; set; }
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    [TableName("Insurances")]
    [PrimaryKey("InsuranceID")]
    public class Insurance
    {
        public long InsuranceID { get; set; }
        public string PolicyNo { get; set; }
        public long ReceiptID { get; set; }
        public long MemberID { get; set; }
        [ResultColumn]
        public long MemberNo { get; set; }
        [ResultColumn]
        public string MemberName { get; set; }
        [ResultColumn]
        public string MobileNo { get; set;}
        [ResultColumn]
        public string NomineeName { get; set; }
        [ResultColumn]
        public string Relationship { get; set; }
        public double PaidAmount { get; set; }
        public string AmountInWords { get; set; }
        public string PaymentMode { get; set; }
        public DateTime ReceiptDate { get; set; }
        public int Status { get; set; }
    }

    [TableName("Users")]
    [PrimaryKey("UserID")]
    public class User
    {
        public long UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNo { get; set; }
    }

    [TableName("InitialNumbers")]
    [PrimaryKey("InitialID")]
    public class InitialNumber {
        public long InitialID { get; set; }
        public string ResourceType { get; set; }
        public string Prefix { get; set; }
        public string Sufix { get; set; }
        public long LastNo { get; set; }
        public int IsIncrement { get; set; }
        public int Digit { get; set; }
        public string PadText { get; set; }
        public int IsPadded { get; set; }
        public string Description { get; set; }
        public string CreatedUser {get; set;}
        public DateTime CreatedDate { get; set; }
        
    }

    [TableName("Screencustomization")]
    [PrimaryKey("ScreenID")]
    public class Screencustomization {
        public long ScreenID { get; set; }
        public string ScreenName { get; set; }
        public string Segment { get; set; }
        public string CustValue { get; set; }
        public string Description { get; set; }

    }

    [TableName("Accounts")]
    [PrimaryKey("ReceiptID")]
    public class Account
    {
        public long AccountID { get; set; }
        public long AccountNo { get; set; }
        public long ReceiptID { get; set; }
        public long InsuranceID { get; set; }
        public long IncomeBillNo { get; set; }
        public long ExpenseBillNo { get; set; }
        public long ReceiptBillNo { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public string AmountInWords { get; set; }
        public string Reason { get; set; }
        public string PaymentMode { get; set; }
        public string PaymentType { get; set; }
        public string TransactionType { get; set; }
        public long CategoryID { get; set; }
        public string OtherCategoryDesc { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
        public DateTime? ReceiptDate { get; set; }
        public int Status { get; set; }
        public string MobileNo { get; set; }
        public string ApprovedBy { get; set; }
        public string PaidBy { get; set; }
        [ResultColumn]
        public string CategoryName { get; set; }
    }

    [TableName("Zonals")]
    [PrimaryKey("ZonalID")]
    public class Zonal
    {
        public long ZonalID { get; set; }
        public string ZonalName { get; set; }
        public int OrderBy { get; set; }
        public int Status { get; set; }
    }

    [TableName("ZonalAreas")]
    [PrimaryKey("ZonalAreaID")]
    public class ZonalArea
    {
        public long ZonalAreaID { get; set; }
        public string ZonalAreaName { get; set; }
        public long ZonalID { get; set; }
        public int Status { get; set; }
    }

    [TableName("ZonalsubAreas")]
    [PrimaryKey("ZonalSubAreaID")]
    public class ZonalSubArea
    {
        public long ZonalSubAreaID { get; set; }
        public string ZonalSubAreaName { get; set; }
        public long ZonalID { get; set; }
        public long ZonalAreaID { get; set; }
        public int Status { get; set; }
    }


    [TableName("AreaPincodes")]
    [PrimaryKey("AreaPincodeID")]
    public class AreaPincode {
        public long AreapincodeID { get; set; }
        public string Areapincode { get; set; }
        public long ZonalAreaID { get; set; }
        public long ZonalSubAreaID { get; set; }
        public long ZonalID { get; set; }
        public int Status { get; set; }
    }

    [TableName("Categorys")]
    [PrimaryKey("CategoryID")]
    public class Category {
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryType { get; set; }
        public int Status { get; set; }
    }

    [TableName("Magazines")]
    [PrimaryKey("MagazineID")]
    public class Magazine{
        public long MagazineID { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string TelephoneNo { get; set; }
        public string Address { get; set; }
        public string SubArea {get;set;}
        public string Area { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public int Status { get; set; }


    }

    [TableName("Meetings")]
    [PrimaryKey("MeetingID")]
    public class Meeting
    {
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public DateTime MeetingDateTime { get; set; }
       // public string MeetingTime { get; set; }
        public string MeetingPlace { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public string SmsDescription { get; set; }
        public string SendSmsTo { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        [ResultColumn]
        public List<MemberEntry> MemberEntries { get; set; }

        public string MembersArea { get; set; }

    }

    [TableName("MemberEntries")]
    [PrimaryKey("MemberEntryID")]
    public class MemberEntry
    {
        public long MemberEntryID { get; set; }
        public long MemberID { get; set; }
        public long MeetingID { get; set; }
        [ResultColumn]
        public long MemberNo { get; set; }
        [ResultColumn]
        public string MemberName { get; set; }
        [ResultColumn]
        public string MobileNo { get; set; }
        [ResultColumn]
        public string StoreName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int Status { get; set; }
        [ResultColumn]
        public string MeetingTitle { get; set; }
        [ResultColumn]
        public string MeetingDate { get; set; }
        [ResultColumn]
        public string MeetingPlace { get; set; }
        //[ResultColumn]
        // public string Message { get; set; }
    }

    [TableName("RoomTransfers")]
    [PrimaryKey("RoomTransferID")]
    public class RoomTransfer
    {
        public long RoomTransferID { get; set; }
        public int MemberID { get; set; }
        public int FromRoomNo { get; set; }
        public int FromLockerNo { get; set; }
        public int ToRoomNo { get; set; }
        public int ToLockerNo { get; set; }
        public DateTime TransferredAt { get; set; }
        public long UserID { get; set; }
        [ResultColumn]
        public long MemberNo { get; set; }
    }

}