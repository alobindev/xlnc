﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.Models;

namespace Xlnc.Models
{
    public class RoomsListViewModel
    {
        public List<Room> Rooms { get; set; }
    }
}