﻿using Xlnc.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xlnc.Models
{
    public class MeetingListViewModel
    {
            public MeetingListViewModel()
            {
                this.PageNumber = PageUtils.PAGE_NUMBER;
                this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
                this.SearchColumnCondition = FilterUtils.OR_CONDITION;
                this.SearchColumnNames = "MeetingTitle,MeetingDate";
            }

            public string Query { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string PaymentType { get; set; }
            public string SearchColumnNames { get; set; }
            public string SearchColumnCondition { get; set; }
            public string SearchFilterOperator { get; set; }
            public string[] FilterColumnValues { get; set; }
            public string[] FilterOperators { get; set; }
            public string[] FilterColumnValues2 { get; set; }
            public string[] SortColumnNames { get; set; }
            public string[] SortColumnDirs { get; set; }
            public long PageNumber { get; set; }
            public long ItemsPerPage { get; set; }
            public PetaPoco.Page<Meeting> Meetings { get; set; }
            
    }
    public class MeetingEditViewModel
    {
         public  Meeting MeetingData { get; set; }
    }
}