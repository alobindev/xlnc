﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using Xlnc.Models;
using System.Text;
using Xlnc.Utils;
using System.IO;
using System.Net;

namespace Xlnc.Controllers
{
    public class MembersController : Controller
    {
        private MemberBAL memberBAL = new MemberBAL();
        private InitialNoBAL initialnoBAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MembersController));
        // private static string Member_Image_Path = ControllerUtils.GetScreenData("Member", "Member_Image_Upload");
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        private static String Payment_Date = ControllerUtils.GetScreenData("MemberExpiry", "Payment_Last_Date");
        //
        // GET: /Members/
        public ActionResult Index(MemberListViewModel memberListViewModel, Member member)
        {

            var memberFilter = memberListViewModel.MemberFilter;
            if (memberFilter == null)
            {
                memberFilter = new MemberFilerModel();
            }
            if (!String.IsNullOrEmpty(memberFilter.FromDate))
            {
                memberFilter.FromDateTime = DateTime.ParseExact(memberFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(memberFilter.ToDate))
            {
                memberFilter.ToDateTime = DateTime.ParseExact(memberFilter.ToDate, "dd/MM/yyyy", null);
            }
            memberListViewModel.MemberFilter = memberFilter;
            memberListViewModel.Members = memberBAL.PagedQuery(memberListViewModel);
            return View(memberListViewModel);
            /*memberListViewModel.Members = memberBAL.PagedQuery(memberListViewModel);
            return View(memberListViewModel);*/
        }

        // GET: /Members/Create
        public ActionResult Create(int RoomNo = 0, int LockerNo = 0)//string Reference)
        {
            var member = new Member();
            member.RoomNo = RoomNo;
            member.LockerNo = LockerNo;
            MemberEditViewModel memberEditViewModel = new MemberEditViewModel   
            {
                MemberData = member

            };
            return View(memberEditViewModel);
        }

        // GET: /Members/Edit/1
        public ActionResult Edit(long? MemberID)
        {
            if (MemberID != null)
            {
                long ID = (long)MemberID;
                Member member = this.memberBAL.FindByID(ID);
                MemberEditViewModel memberEditViewModel = new MemberEditViewModel
                {
                    MemberData = member
                };
                //if (member.MemberID > 0) {
                //    logger.Info("Inside ShowImage MemberID = "+member.MemberID + ", PhotoURL = "+member.PhotoURL );
                //    return RedirectToAction("ShowImage","Members", new{ID = member.MemberID,PhotoURL = member.PhotoURL});
                //}
                return View("Create", memberEditViewModel);
            }
            return RedirectToAction("Create", "Members");
        }
        public FileResult ShowImage(long ID, string PhotoURL)
        {
            var Member_Image_Path = ControllerUtils.GetScreenData("Member", "Member_Image_Upload");
            var fileType = "image/jpg";
            if (ID > 0)
            {
                // logger.Info("inside ShowImage, FileName = " + FileName);

                logger.Info("fileType = " + fileType + ", ImageURL = " + PhotoURL);
                return File(Member_Image_Path + PhotoURL, fileType);
            }
            else if (ID == 0)
            {
                logger.Info("inside ShowImage, FileName = " + Member_Image_Path + "dummyuser.jpeg" + fileType);
                return File(Member_Image_Path + "dummyuser.jpg", fileType);
            }
            return null;
        }

        [HttpPost]
        public ActionResult Create(Member member)
        {
            //  long memberid = member.MemberID;
            logger.Info("ScanFile = " + member.PhotoName);
            Member memberid = this.memberBAL.FindByID(member.MemberID);

            try
            {
                if (member.MemberID > 0)
                {
                    logger.Info("Inside Update Member MemberID =" + member.MemberID);
                    this.memberBAL.Update(member);
                    //ViewBag.SuccessMessage = "Record Updated Successfully";
                    Auth.SetStatusMessage("Member Updated Successfully");
                }
                else
                {
                    this.memberBAL.Save(member);
                    //ViewBag.SuccessMessage = "Record Saved Successfully";
                    Auth.SetStatusMessage("Member Saved Successfully");
                }
            }
            catch (Exception e)
            {
                logger.Info("Error in save Member" + e.Message);
                Auth.SetStatusMessage("Problem in Saving Member, please try again.", "Warning");
            }

            return new RedirectResult("Index");
        }

        public JsonResult Delete(long ID)
        {
            logger.Info("Inside Delete Member");
            //if (ID > 0)
            //{
            logger.Info("Delete Member " + ID);
            //this.memberBAL.Delete(ID);
            return Json(this.memberBAL.Delete(ID), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            //}
            //else
            //    ViewBag.Message = "Member is not available to delete.";
            //  return new RedirectResult("Index");
        }

        public JsonResult GetBookedMembers(string filter)
        {
            return Json(this.memberBAL.GetBookedMembers(filter), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Dismiss(long ID, string Reason)
        {
            logger.Info("Inside Dismiss Member");
            logger.Info("Dismiss Member " + ID);
            return Json(this.memberBAL.Dismiss(ID, Reason), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Search(MemberListViewModel memberListViewModel)
        {
            memberListViewModel.Members = memberBAL.PagedQuery(memberListViewModel); // Todo: for future filter operation: memberListViewModel
            return View("Index", memberListViewModel);
        }

        // Member Address print and List print
        public ActionResult MembersPrint(string Query, string PrintOption)
        {
            List<Member> memberdetails = new List<Member>();
            memberdetails = this.memberBAL.MembersPrint(Query, PrintOption);
            if (PrintOption == "MembersList")
            {
                logger.Info("Inside 'MembersPrint' Print Member List");
                return View("MembersListPrint", memberdetails);
            }
            else if (PrintOption == "MembersAddressWithImage")
            {
                logger.Info("Inside 'MemberAddressWithImage' Print Member Address");
                return View("MembersAddressPrintWithImage", memberdetails);
            }
            else if (PrintOption == "MembersAddress")
            {
                logger.Info("Inside 'MembersPrint' Print Member Address");
                return View("MembersAddressPrint", memberdetails);
            }
            else if (PrintOption == "MembersDismissedList")
            {
                logger.Info("Inside 'MembersPrint' Print Members Dismissed List ");
                return View("MembersDismissedListPrint", memberdetails);
            }
            else
            {
                return View();
            }
        }

        public ActionResult MembersSinglePrint(long MemberID)
        {
            try
            {
                Member memberid = new Member();

                if (MemberID > 0)
                {
                    logger.Info("Inside MembersSingleprint MemberID = " + MemberID);
                    memberid = this.memberBAL.FindByID(MemberID);

                }
                logger.Info("MembersSingleprint Result" + memberid.ToString());
                MemberEditViewModel memberEditViewModel = new MemberEditViewModel
                {
                    MemberData = memberid
                };

                return View("MembersSinglePrint", memberEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ReceiptSinglePrint" + e.Message);
            }
            return View("MembersSinglePrint", new MemberEditViewModel());
        }

        public ActionResult MembersSinglePrintReport(long MemberNo)
        {
            try
            {
                Member memberid = new Member();

                if (MemberNo > 0)
                {
                    logger.Info("Inside MembersSingleprint MemberNo = " + MemberNo);
                    memberid = this.memberBAL.FindByMemberNo(MemberNo);

                }
                logger.Info("MembersSingleprint Result" + memberid.ToString());
                MemberEditViewModel memberEditViewModel = new MemberEditViewModel
                {
                    MemberData = memberid
                };

                return View("MembersSinglePrint", memberEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ReceiptSinglePrint" + e.Message);
            }
            return View("MembersSinglePrint", new MemberEditViewModel());
        }

        public ActionResult GetMemberAutoComplete(string ColumnName, string term)
        {
            return Json(memberBAL.GetMemberAutoComplete(ColumnName, term), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMemberExpiryList()
        {
            List<Member> expirycount = new List<Member>();
            string result = "";
            try
            {
                expirycount = this.memberBAL.GetMembersExpiry();
                var count = 0;
                var mobilecount = 0;
                var unsentsmscount = 0;
                foreach (var expiry in expirycount)
                {
                    if (expiry.MembershipExpiryDate <= DateTime.Now.Date.AddDays(+1))
                    {
                        logger.Info("Send SMS to Membership expired customers =" + expiry.MobileNo);
                        if (Send_Sms_To_Members.Equals("Yes"))
                        {
                            string PaymentDate = Payment_Date + "/" + DateTime.Now.Year;
                            string expirymessage = "பா.குப்புசாமி புதுபேட்டை சங்கம், உங்கள் சந்தா முடிவடைந்துவிட்டது, " + PaymentDate + " க்குள் புதுபித்து கொள்ளவும்";
                            string mobileno = expiry.MobileNo;
                            //DateTime ExpiryDate = ParseEx expiry.MembershipExpiryDate.ToString("dd/MM/yyyy");
                            logger.Info("Membership Expiry Alert SMS Before 1Week. Date Before 1week = " + DateTime.Now.Date.AddDays(+7) + " - ExpiryDate = " + expiry.MembershipExpiryDate);
                            logger.Info("பா.குப்புசாமி புதுபேட்டை சங்கம், உங்கள் சந்தா முடிவடைந்துவிட்டது, " + PaymentDate + " க்குள் புதுபித்து கொள்ளவும்");
                            if ((mobileno != null) && (mobileno != ""))
                            {
                                //    logger.Info("Inside mobileno " + mobileno.Length);
                                string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + mobileno + "&priority=2&sender=MCPPNS&message=" + expirymessage;

                                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                                webReq.Method = "GET";
                                HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                                //I don't use the response for anything right now. But I might log the response answer later on.   
                                Stream answer = webResponse.GetResponseStream();
                                StreamReader _recivedAnswer = new StreamReader(answer);
                                // logger.Info("Response For Message = " +_recivedAnswer );
                                TempData["Success"] = "SMS Sent Successfully";
                                mobilecount++;

                            }

                        }
                        count++;
                        unsentsmscount = count - mobilecount;
                        result = "SMS sent successfully for " + mobilecount + " Members. " + unsentsmscount + " Members doesn't have proper mobile number";
                    }

                }
                logger.Info("Total Membershipexipry count = " + count + " Mobile Count = " + mobilecount);

            }
            catch (Exception e)
            {

                logger.Info("Error in Membership expiry count " + e.Message);
            }
            return Json(result, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SendBirthdaySMS()
        {
            List<Member> memberdetails = new List<Member>();
            memberdetails = this.memberBAL.GetMembersExpiry();
            List<string> result = new List<string>();
            string membername = "";
            long memberid = 0;
            foreach (var sendsms in memberdetails)
            {

                string mobileno = sendsms.MobileNo;
                string name = sendsms.MemberName;
                string birthdaymessage = "பா.குப்புசாமி புதுபேட்டை சங்கம், திரு. " + name + " அவர்களுக்கு இனிய பிறந்தநாள் நல்வாழ்த்துக்கள்";
                string birthdate = sendsms.DateOfBirth.Value.ToString("dd-MMMM");
                string currentdate = DateTime.Now.ToString("dd-MMMM");
                if (Send_Sms_To_Members == "Yes")
                {
                    if (birthdate == currentdate)
                    {

                        logger.Info("Members Birthday SMS  Date " + sendsms.DateOfBirth);
                        logger.Info("Happy Birthday to  Mr. " + name);
                        if ((mobileno != null) && (mobileno != ""))
                        {
                            logger.Info("Inside mobileno Length " + mobileno.Length + " MobileNo = " + mobileno);
                            string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + mobileno + "&priority=2&sender=MCPPNS&message=" + birthdaymessage;

                            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                            webReq.Method = "GET";
                            HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                            //I don't use the response for anything right now. But I might log the response answer later on.   
                            Stream answer = webResponse.GetResponseStream();
                            StreamReader _recivedAnswer = new StreamReader(answer);
                            membername = name;
                            memberid = sendsms.MemberNo;
                            result.Add("SMS sent to Mr. " + sendsms.MemberName + " MemberID " + sendsms.MemberNo + "\n");
                            logger.Info("Result = " + result);

                        }

                    }

                }


            }
            return Json(result, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMember(long MemberID)
        {
            return Json(this.memberBAL.FindByID(MemberID), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMemberByMemberNo(long MemberNo)
        {
            return Json(this.memberBAL.GetMemberByMemberNo(MemberNo), JsonRequestBehavior.AllowGet);
        }
    }
}