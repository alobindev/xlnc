﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using Xlnc.Models;

namespace Xlnc.Controllers
{
    public class ReportController : Controller
    {
        private AccountBAL accountBAL = new AccountBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DashboardController));


        public ActionResult Index(AccountListViewModel accountListViewModel)
        {
            accountListViewModel.Accounts = accountBAL.GetGroupByIncomeAndExpenseDetails(accountListViewModel);
            return View(accountListViewModel);
        }

        public ActionResult GetGroupByIncomeAndExpenseDetails(AccountListViewModel accountListViewModel)
        {
            //  List<Account> accountdetails = new List<Account>();
            try
            {
                accountListViewModel.Accounts = accountBAL.GetGroupByIncomeAndExpenseDetails(accountListViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetIncomeAndExpenseDetails = " + e.Message);
            }

            return View("Index", accountListViewModel);
        }
    }
}