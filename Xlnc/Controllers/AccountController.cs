﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using Xlnc.Models;
using Xlnc.Utils;
using System.Text;
using System.IO;
using System.Net;


namespace Xlnc.Controllers
{
    public class AccountController : Controller
    {
        private AccountBAL accountBAL = new AccountBAL();
        private CategoryBAL categoryBAL = new CategoryBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountController));
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        // GET: /Account/


        public ActionResult IncomeIndex(AccountListViewModel accountListViewModel)
        {
            /*logger.Info("Inside DailyIncome");
            
            accountListViewModel.Accounts = accountBAL.IncomePagedQuery(accountListViewModel);
            return View(accountListViewModel);*/

            var accountFilter = accountListViewModel.AccountFilter;
            if (accountFilter == null)
            {
                accountFilter = new AccountFilterModel();
            }
            if (!String.IsNullOrEmpty(accountFilter.FromDate))
            {
                accountFilter.FromDateTime = DateTime.ParseExact(accountFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(accountFilter.ToDate))
            {
                accountFilter.ToDateTime = DateTime.ParseExact(accountFilter.ToDate, "dd/MM/yyyy", null);
            }
            accountFilter.AccountType = "Credit";
            accountListViewModel.AccountFilter = accountFilter;
            accountListViewModel.Accounts = accountBAL.PagedQuery(accountListViewModel);// IncomePagedQuery(accountListViewModel);
            return View(accountListViewModel);

        }
        public ActionResult ExpenseIndex(AccountListViewModel accountListViewModel)
        {
            /*logger.Info("Inside DailyIncome");
            accountListViewModel.Accounts = accountBAL.ExpensePagedQuery(accountListViewModel);
            return View(accountListViewModel);*/

            var accountFilter = accountListViewModel.AccountFilter;
            if (accountFilter == null)
            {
                accountFilter = new AccountFilterModel();
            }
            if (!String.IsNullOrEmpty(accountFilter.FromDate))
            {
                accountFilter.FromDateTime = DateTime.ParseExact(accountFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(accountFilter.ToDate))
            {
                accountFilter.ToDateTime = DateTime.ParseExact(accountFilter.ToDate, "dd/MM/yyyy", null);
            }
            accountFilter.AccountType = "Debit";
            accountListViewModel.AccountFilter = accountFilter;
            accountListViewModel.Accounts = accountBAL.PagedQuery(accountListViewModel); //ExpensePagedQuery(accountListViewModel);
            return View(accountListViewModel);
        }

        public ActionResult IncomeCreate(AccountEditViewModel accountEditViewModel)
        {
            logger.Info("Inside DailyIncome");
            var categorytype = "Account";
            var categories = this.categoryBAL.GetCategoriesByType(categorytype);
            accountEditViewModel = new AccountEditViewModel
            {
                AccountData = new Account(),
                Categorys = categories
            };
            return View(accountEditViewModel);
        }
        public ActionResult ExpenseCreate(AccountEditViewModel accountEditViewModel)
        {
            logger.Info("Inside DailyIncome");
            var categorytype = "Account";
            var categories = this.categoryBAL.GetCategoriesByType(categorytype);
            accountEditViewModel = new AccountEditViewModel
            {
                AccountData = new Account(),
                Categorys = categories
            };
            return View(accountEditViewModel);
        }

        public ActionResult IncomeEdit(AccountListViewModel accountListViewModel)
        {
            logger.Info("Inside DailyIncome");
            accountListViewModel.Accounts = accountBAL.IncomePagedQuery(accountListViewModel);
            return View(accountListViewModel);

        }
        [HttpPost]
        public ActionResult Create(Account account)
        {
            var viewpage = "";
            try
            {
                if (account.AccountID > 0)
                {
                    logger.Info("AccountID = " + account.ReceiptID);
                    account.Status = 1;
                    this.accountBAL.Update(account);
                }
                else
                {
                    account.Status = 1;
                    this.accountBAL.Save(account);
                }
               
                if (account.PaymentType == "Credit")
                {
                    viewpage = "IncomeIndex";
                    Auth.SetStatusMessage("Income Receipt Saved Successfully");
                }
                else
                {
                    viewpage = "ExpenseIndex";
                    Auth.SetStatusMessage("Expense Receipt Saved Successfully");
                }
            }
            catch (Exception e)
            {
                logger.Info("Error in Create Income" + e.Message);
                Auth.SetStatusMessage("Problem in Saving Receipt, please try again.", "Warning");
            }
           
            return new RedirectResult(viewpage);
        }

        //[HttpPost]
        public ActionResult Edit(long? AccountID)
        {
            var viewpage ="";
            if (AccountID != null)
            {
                long ID = (long)AccountID;
                Account account = this.accountBAL.FindByID(ID);
                var categories = this.categoryBAL.GetCategoriesByType("Account");
                AccountEditViewModel accountEditViewModel = new AccountEditViewModel
                {
                    AccountData = account,
                    Categorys = categories
                };

                if (account.PaymentType == "Credit")
                {
                    viewpage = "IncomeCreate";
                }
                else {
                    viewpage = "ExpenseCreate";
                }
                return View(viewpage, accountEditViewModel);
            }
            return RedirectToAction("IncomeIndex","Acount");
        }


        public ActionResult Search(AccountListViewModel accountListViewModel)
        {
            var accountFilter = accountListViewModel.AccountFilter;
            if (accountFilter == null)
            {
                accountFilter = new AccountFilterModel();
            }
            if (!String.IsNullOrEmpty(accountFilter.FromDate))
            {
                accountFilter.FromDateTime = DateTime.ParseExact(accountFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(accountFilter.ToDate))
            {
                accountFilter.ToDateTime = DateTime.ParseExact(accountFilter.ToDate, "dd/MM/yyyy", null);
            }
            accountListViewModel.AccountFilter = accountFilter;
            var viewpage = "";
            if (!String.IsNullOrEmpty(accountFilter.AccountType) && accountFilter.AccountType.Equals("Credit"))
                viewpage = "IncomeIndex";
            else if (!String.IsNullOrEmpty(accountFilter.AccountType) && accountFilter.AccountType.Equals("Debit"))
                viewpage = "ExpenseIndex";

            accountListViewModel.Accounts = accountBAL.PagedQuery(accountListViewModel);
            return View(viewpage, accountListViewModel);
        }

        /*public ActionResult Search(AccountListViewModel accountListViewModel)
        {
            logger.Info("PaymentType = "+accountListViewModel.PaymentType);
            var viewpage = "";
            if (accountListViewModel.PaymentType == "Credit")
            {
                logger.Info("Credit ");
                accountListViewModel.Accounts = accountBAL.IncomePagedQuery(accountListViewModel);
                viewpage = "IncomeIndex";
            }
            else if (accountListViewModel.PaymentType == "Debit")
            {
              logger.Info("Debit ");
                accountListViewModel.Accounts = accountBAL.ExpensePagedQuery(accountListViewModel);
                viewpage = "ExpenseIndex";
            }
            return View(viewpage, accountListViewModel);
        }*/

        public JsonResult Delete(long ID)
        {
            logger.Info("Delete Account " + ID);
            Account account = this.accountBAL.FindByID(ID);
            if (account != null)
                this.accountBAL.Delete(account);
            else
                ViewBag.Message = "Receipt is not available to delete.";
            return Json(account, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

        public ActionResult AccountsPrint(AccountListViewModel accountListViewModel)
        {
            /*List<Account> incomereceiptdetails = new List<Account>();
            incomereceiptdetails = this.accountBAL.AccountsPrint(accountListViewModel);
            logger.Info("Inside IncomeReceiptPrint Print Receipt List");
            return View("IncomeReceiptListPrint", incomereceiptdetails);*/
            List<Account> incomereceiptdetails = new List<Account>();
            var accountFilter = accountListViewModel.AccountFilter;
            if (accountFilter == null)
            {
                accountFilter = new AccountFilterModel();
            }
            if (!String.IsNullOrEmpty(accountFilter.FromDate))
            {
                accountFilter.FromDateTime = DateTime.ParseExact(accountFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(accountFilter.ToDate))
            {
                accountFilter.ToDateTime = DateTime.ParseExact(accountFilter.ToDate, "dd/MM/yyyy", null);
            }
            accountListViewModel.AccountFilter = accountFilter;
            incomereceiptdetails = this.accountBAL.AccountsPrint(accountListViewModel);

            var viewpage = "";
            if (!String.IsNullOrEmpty(accountFilter.AccountType) && accountFilter.AccountType.Equals("Credit"))
                viewpage = "IncomeReceiptListPrint";
            else if (!String.IsNullOrEmpty(accountFilter.AccountType) && accountFilter.AccountType.Equals("Debit"))
                viewpage = "ExpenseListPrint";
            
            return View(viewpage, incomereceiptdetails);
        }

        public ActionResult IncomeReceiptPrint(string Query, string PrintOption, string FromDate, string ToDate)
        {
            List<Account> incomereceiptdetails = new List<Account>();
            incomereceiptdetails = this.accountBAL.IncomeReceiptPrint(Query, FromDate, ToDate);
            logger.Info("Inside IncomeReceiptPrint Print Receipt List");
            return View("IncomeReceiptListPrint", incomereceiptdetails);

        }
        public ActionResult IncomeReceiptSinglePrint(long ReceiptID)
        {
            try
            {
                Account incomeid = new Account();

                if (ReceiptID > 0)
                {
                    logger.Info("Inside IncomeReceiptSinglePrint ReceiptID = " + ReceiptID);
                    incomeid = this.accountBAL.FindByID(ReceiptID);

                }
                logger.Info("IncomeReceiptSinglePrint Result" + incomeid.ToString());
                AccountEditViewModel memberEditViewModel = new AccountEditViewModel
                {
                    AccountData = incomeid
                };

                return View("IncomeReceiptSinglePrint", memberEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ReceiptSinglePrint" + e.Message);
            }
            return View("IncomeReceiptSinglePrint", new AccountEditViewModel());
        }

        public ActionResult IncomeSinglePrintByBillNo(long BillNo)
        {
            try
            {
                Account incomeid = new Account();

                if (BillNo > 0)
                {
                    logger.Info("Inside IncomeSinglePrintByBillNo ReceiptID = " + BillNo);
                    incomeid = this.accountBAL.FindByIncomeBillNo(BillNo);

                }
                logger.Info("IncomeReceiptSinglePrint Result" + incomeid.ToString());
                AccountEditViewModel memberEditViewModel = new AccountEditViewModel
                {
                    AccountData = incomeid
                };

                return View("IncomeReceiptSinglePrint", memberEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in IncomeSinglePrintByBillNo" + e.Message);
            }
            return View("IncomeReceiptSinglePrint", new AccountEditViewModel());
        }

        public ActionResult ExpenseListPrint(string Query, string PrintOption, string FromDate, string ToDate)
        {
            List<Account> expensedetails = new List<Account>();
            expensedetails = this.accountBAL.ExpenseReceiptPrint(Query, FromDate, ToDate);
            logger.Info("Inside IncomeReceiptPrint Print Receipt List");
            return View("ExpenseListPrint", expensedetails);
        }

        public ActionResult ExpenseSinglePrint(long ReceiptID)
        {
            try
            {
                Account account = new Account();

                if (ReceiptID > 0)
                {
                    logger.Info("Inside VoucherSinglePrint MemberID = " + ReceiptID);
                    account = this.accountBAL.FindByID(ReceiptID);

                }
                logger.Info("VoucherSinglePrint Result" + account.ToString());
                AccountEditViewModel accountEditViewModel = new AccountEditViewModel
                {
                    AccountData = account
                };

                return View("ExpenseSinglePrint", accountEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ExpenseVoucherSinglePrint" + e.Message);
            }
            return View("ExpenseSinglePrint", new AccountEditViewModel());
        }

        public ActionResult ExpenseSinglePrintByBillNo(long BillNo)
        {
            try
            {
                Account account = new Account();

                if (BillNo > 0)
                {
                    logger.Info("Inside VoucherSinglePrint MemberID = " + BillNo);
                    account = this.accountBAL.FindByExpenseBillNo(BillNo);

                }
                logger.Info("VoucherSinglePrint Result" + account.ToString());
                AccountEditViewModel accountEditViewModel = new AccountEditViewModel
                {
                    AccountData = account
                };

                return View("ExpenseSinglePrint", accountEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ExpenseVoucherSinglePrint" + e.Message);
            }
            return View("ExpenseSinglePrint", new AccountEditViewModel());
        }
    }
}