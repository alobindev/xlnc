﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using Xlnc.Models;
using AloFramework.Core.BALs;
using Xlnc.Utils;
using System.Text;
using log4net;
using System.IO;
using System.Net;

namespace Xlnc.Controllers
{
    public class RoomController : Controller
    {

        private RoomBAL roomBAL = new RoomBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RoomController));

        public ActionResult Index()
        {
            var roomsListViewModel = new RoomsListViewModel();
            roomsListViewModel.Rooms = new List<Room>();
            roomsListViewModel.Rooms = this.roomBAL.GetRooms();
            return View(roomsListViewModel);
        }

        public JsonResult GetAvailableRooms(string filter)
        {
            return Json(this.roomBAL.GetAvailableRooms(filter), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllRooms(string filter)
        {
            return Json(this.roomBAL.GetAllRooms(filter), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOccupiedRooms(string filter)
        {
            return Json(this.roomBAL.GetOccupiedRooms(filter), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Transfer()
        {
            RoomTransferEditViewModel roomTransferEditModel = new RoomTransferEditViewModel();
            return View(roomTransferEditModel);
        }

        [HttpPost]
        public ActionResult MemberRoomTransfer(RoomTransferEditViewModel roomTransferEditModel)
        {
            this.roomBAL.RoomTransfer(roomTransferEditModel);
            return View("Transfer", roomTransferEditModel);
        }
    }
}