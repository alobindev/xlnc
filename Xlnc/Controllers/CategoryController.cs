﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using Xlnc.Models;
using AloFramework.Core.BALs;
using Xlnc.Utils;
using System.Text;

namespace Xlnc.Controllers
{
    public class CategoryController : Controller
    {
        private CategoryBAL categoryBAL = new CategoryBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountController));
        // GET: Category

        public ActionResult Index(CategoryListViewModel categoryListViewModel)
        {
            categoryListViewModel.Categories = categoryBAL.PagedQuery(categoryListViewModel);
            return View(categoryListViewModel);
        }

        public ActionResult Create() {
            CategoryEditViewModel categoryEditViewModel = new CategoryEditViewModel
            {
                CategoryData = new Category()
            };
            return View(categoryEditViewModel);
        }

        

        [HttpPost]
        public ActionResult Create(Category category) {
            try
            {
                if (category.CategoryID > 0)
                {
                    this.categoryBAL.Update(category);
                    Auth.SetStatusMessage("Record Updated Successfully");
                }
                else
                {
                    category.Status = 1;
                    this.categoryBAL.Save(category);

                    Auth.SetStatusMessage("Record Saved Successfully");
                }
            }
            catch (Exception e) {
                logger.Info("Error in save Category" + e.Message);
            }
            return new RedirectResult("Index");
        }

        public ActionResult Search(CategoryListViewModel categoryListViewModel)
        {
            categoryListViewModel.Categories = categoryBAL.PagedQuery(categoryListViewModel);
            return View("Index", categoryListViewModel);
        }


        public ActionResult Edit(long? CategoryID)
        {
            if (CategoryID != null)
            {
                long ID = (long)CategoryID;
                Category category= this.categoryBAL.FindByID(ID);
                CategoryEditViewModel categoryEditViewModel = new CategoryEditViewModel
                {
                    CategoryData = category
                };
                return View("Create", categoryEditViewModel);
            }
            return RedirectToAction("Create", "Category");
        }

        public JsonResult Delete(long CategoryID)
        {
            return Json(this.categoryBAL.Delete(CategoryID), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

    }
}