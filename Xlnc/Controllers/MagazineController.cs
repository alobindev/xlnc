﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using Xlnc.Models;
using AloFramework.Core.BALs;
using Xlnc.Utils;
using System.Text;
using log4net;
using System.IO;
using System.Net;

namespace Xlnc.Controllers
{
    public class MagazineController : Controller
    {
        private TranslationBAL _transactionBAL = new TranslationBAL();
        private MemberBAL memberBAL = new MemberBAL();
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceController));
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        //
        // GET: /Insurance/
        private MagazineBAL magazineBAL = new MagazineBAL();
        //
        // GET: /Insurance/
        public ActionResult Index(MagazineListViewModel magazineListViewModel)
        {
            magazineListViewModel.Magazines = magazineBAL.PagedQuery(magazineListViewModel);
            return View(magazineListViewModel);
        }

        
        public ActionResult Search(MagazineListViewModel magazineListViewModel)
        {
            magazineListViewModel.Magazines = magazineBAL.PagedQuery(magazineListViewModel);
            return View("Index", magazineListViewModel);
        }

        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Magazine magazine)
        {
          
            try
            {
                if (magazine.MagazineID > 0)
                {
                    logger.Info("Inside Update Member MemberID =" + magazine.MagazineID);
                    this.magazineBAL.Update(magazine);
                    //ViewBag.SuccessMessage = "Record Updated Successfully";
                    Auth.SetStatusMessage("Record Updated Successfully");
                }
                else
                {
                    this.magazineBAL.Save(magazine);
                    //ViewBag.SuccessMessage = "Record Saved Successfully";
                    Auth.SetStatusMessage("Record Saved Successfully");
                }
            }
            catch (Exception e)
            {
                logger.Info("Error in save Magazine" + e.Message);
                Auth.SetStatusMessage("Problem in Saving Non Member, please try again.", "Warning");
            }

            return new RedirectResult("Index");
        }

        public ActionResult Edit(long? MagazineID)
        {
            if (MagazineID != null)
            {
                long ID = (long)MagazineID;
                Magazine magazine = this.magazineBAL.FindByID(ID);
                MagazineEditViewModel magazineEditViewModel = new MagazineEditViewModel
                {
                    MagazineData = magazine
                };
                //if (member.MemberID > 0) {
                //    logger.Info("Inside ShowImage MemberID = "+member.MemberID + ", PhotoURL = "+member.PhotoURL );
                //    return RedirectToAction("ShowImage","Members", new{ID = member.MemberID,PhotoURL = member.PhotoURL});
                //}
                return View("Edit", magazineEditViewModel);
            }
            return RedirectToAction("Create", "Magazine");
        }

        public JsonResult Delete(long MagazineID)
        {
            return Json(this.magazineBAL.Delete(MagazineID), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
         
        }

        // Member Address print
        public ActionResult AddressPrint(string Query, string PrintOption)
        {
            List<Magazine> memberdetails = new List<Magazine>();
            memberdetails = this.magazineBAL.AddressPrint(Query, PrintOption);
            if (PrintOption == "MembersList")
            {
                logger.Info("Inside 'MembersPrint' Print Member List");
                return View("MembersListPrint", memberdetails);
            }
            else if (PrintOption == "MembersAddressWithImage")
            {
                logger.Info("Inside 'MemberAddressWithImage' Print Member Address");
                return View("MembersAddressPrintWithImage", memberdetails);
            }
            else if (PrintOption == "MembersAddress")
            {
                logger.Info("Inside 'MembersPrint' Print Member Address");
                return View("MagazineAddressPrint", memberdetails);
            }
            else if (PrintOption == "MembersDismissedList")
            {
                logger.Info("Inside 'MembersPrint' Print Members Dismissed List ");
                return View("MembersDismissedListPrint", memberdetails);
            }
            else
            {
                return View();
            }
        }

    }
}

