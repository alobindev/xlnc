﻿using AloFramework.Core.BALs;
using Xlnc.BALs;
using Xlnc.Models;
using Xlnc.Utils;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
namespace Xlnc.Controllers
{
    public class BackupController  : Controller
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(BackupController));

      
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            try
            {
                logger.Info("inside creating backup");
                var constring = WebConfigurationManager.ConnectionStrings["AloFrameworkContext"].ConnectionString;
                string file = WebConfigurationManager.AppSettings["BackupFilePath"];
                logger.Info("creating backup file in path " + file);
                file += (@"\" + (DateTime.Now.Ticks + ".sql"));
                logger.Info("creating backup file "+ file);
                using (MySqlConnection conn = new MySqlConnection(constring))
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        using (MySqlBackup mb = new MySqlBackup(cmd))
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            mb.ExportToFile(file);
                            conn.Close();
                        }
                    }
                }
                Auth.SetStatusMessage(String.Format(Lang.GetLang("backup_saved"), file));
            }
            catch(Exception e) {
                logger.Info("Error in Backup" + e);
            }
            return RedirectToAction("Index");
        }
       
    }
}