﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using Xlnc.Models;
using System.Text;

namespace Xlnc.Controllers
{
    public class DashboardController : Controller
    {
        private MemberBAL memberBAL = new MemberBAL();
        private ReceiptBAL receiptBAL = new ReceiptBAL();
        private AccountBAL accountBAL = new AccountBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DashboardController));

        public ActionResult Index(AccountListViewModel accountListViewModel)
        {
            logger.Info("Inside Dashboard");

            accountListViewModel.Accounts = accountBAL.GetIncomeAndExpenseDetails(accountListViewModel);
            return View(accountListViewModel);
        }

        public JsonResult GetDashboardCounts()
        {
            var dasboardCountModel = new DashboardCountModel();
            try
            {
                dasboardCountModel.MembersCount = this.memberBAL.GetMembersCount();
                dasboardCountModel.ReceiptsCount = this.receiptBAL.GetReceiptsListCount();
            }
            catch (Exception ex)
            {
                logger.Error("Problem in retriving dashboard counts, due to " + ex);
            }
            return Json(dasboardCountModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMembersCount()
        {
            int membercount = 0;
            try
            {
                membercount = this.memberBAL.GetMembersCount();
            }
            catch { 
                
            }
            return Json(membercount, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetReceiptsCount()
        {
            List<Receipt> receiptcount = new List<Receipt>();
            try
            {
                receiptcount = this.receiptBAL.GetReceiptsCount();
            }
            catch
            {

            }
            return Json(receiptcount, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLifeMemberCount()
        {
            int membercount = 0;
            try
            {
                membercount = this.memberBAL.GetLifeMemberCount();
            }
            catch
            {

            }
            return Json(membercount, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExpiryCount()
        {
            List<Member> expirycount = new List<Member>();
            var totalcount = 0;
            try
            {
                expirycount = this.memberBAL.GetMembersExpiry();
                var count = 0;
                logger.Info("Expiry Count = ");
                foreach (var expiry in expirycount) {
                    //logger.Info("MembershipExpiryDate = " + expiry.MembershipExpiryDate +"Current Date = "+ DateTime.Now.Date.AddDays(+1));
                    if (expiry.MembershipExpiryDate <= DateTime.Now.Date.AddDays(+1)) {
                       
                        count++;
                    }
                }
                totalcount = count;
            }
            catch (Exception e)
            {
                logger.Info("Error in Membership expiry count " + e.Message);
            }
            return Json(totalcount, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIncomeAndExpenseDetails(AccountListViewModel accountListViewModel)
        { 
          //  List<Account> accountdetails = new List<Account>();
            try{
                accountListViewModel.Accounts = accountBAL.GetIncomeAndExpenseDetails(accountListViewModel);
            }
            catch(Exception e){
                logger.Info("Error in GetIncomeAndExpenseDetails = "+e.Message);
            }

            return View("Index", accountListViewModel);
        }

        public ActionResult IncomeAndExpenseDetailsPrint(string FromDate, string ToDate)
        {
         //   AccountListViewModel accountListViewModel = new AccountListViewModel();
            List<Account> accountdetails = new List<Account>();
            try
            {

                //  accountListViewModel.Accounts = accountBAL.GetIncomeAndExpenseDetails(accountListViewModel);
                accountdetails = this.accountBAL.IncomeAndExpenseDetailsPrint(FromDate, ToDate);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetIncomeAndExpenseDetails = " + e.Message);
            }

            return View("IncomeExpensePrint", accountdetails);
        }
        
    




    }
}