﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xlnc.BALs;
using System.Text;

namespace Xlnc.Controllers
{
    public class InitialNoController : Controller
    {
        private InitialNoBAL initialnoBAL = new InitialNoBAL();
        // GET: /InitialNo/
        public ActionResult Index()
        {
            return View();
        }
        
        public JsonResult GetInitialNo(string ResourceType)
        {

            return Json(initialnoBAL.GetInitialNo(ResourceType), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

	}
}