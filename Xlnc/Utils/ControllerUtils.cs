﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xlnc.BALs;
using Xlnc.Models;


namespace Xlnc.Utils
{
    public class ControllerUtils
    {
        private static ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ControllerUtils));
       
        public static String GetScreenData(String ScreenName, String Segment)
        {
            String Value = "";
            try
            {
                Value = screencustomizationBAL.GetScreenData(ScreenName, Segment).CustValue;
                logger.Info("Screen Customization value = " + Value);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetScreenData due to " + e.Message);
            }
            return Value;
        }
    }
}