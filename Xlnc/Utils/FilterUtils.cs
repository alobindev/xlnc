﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xlnc.Utils
{
    public static class FilterUtils
    {
        // Filter Conditions
        public const string OR_CONDITION = " or ";
        public const string AND_CONDITION = " and ";

        // Filter Operators
        public const string CONTAINS = "contains";
        public const string NOT_CONTAINS = "not_contains";
        public const string ENDSWITH = "endsWith";
        public const string STARTSWITH = "startsWith";
        public const string EQ = "=";
        public const string GT = ">";
        public const string LT = ">";
        public const string GE = ">=";
        public const string LE = ">=";
        public const string NE = "<>";
        public const string LIKE = "like";
        public const string NOTLIKE = "not like";

        // clause definition
        public const string WHERE = " where ";
        public const string ORDERBY = " order by ";
    }
}