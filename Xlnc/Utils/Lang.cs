﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xlnc.Utils
{
    public class Lang
    {
        public static Dictionary<string, string> enTexts = new Dictionary<string, string>();

        public Lang()
        {
            LoadTexts();
        }

        public static void LoadTexts()
        {
            if (!enTexts.Any())
            {
               enTexts.Add("backup_saved", "Backup saved successfully filename = {0}");
            }
        }

        public static string GetLang(string Key)
        {
            if (enTexts.Count() == 0)
                LoadTexts();
            return enTexts.ContainsKey(Key) ? enTexts.FirstOrDefault(e => e.Key == Key).Value : Key;
        }
    }
}